package sragen.findtruck;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.vision.text.Line;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sragen.findtruck.adapter.ImagesGridAdapter;
import sragen.findtruck.app.API;
import sragen.findtruck.app.ErrorCallback;
import sragen.findtruck.app.ResponseCallback;
import sragen.findtruck.model.DO;
import sragen.findtruck.model.Manifest;
import sragen.findtruck.services.AndroidLocationServices;
import sragen.findtruck.services.LocationUpdatesBroadcastReceiver;
import sragen.findtruck.services.MyAlarmService;
import sragen.findtruck.utilities.BaseActivity;
import sragen.findtruck.utilities.DialogCancelOrder;
import sragen.findtruck.utilities.DialogCancelText;
import sragen.findtruck.utilities.Utils;

/**
 * Created by adikurniawan on 12/05/18.
 */

public class DetailAngkutActivity extends BaseActivity {
    private LinearLayout container_jumlah_barang, container_foto;
    private CardView btn_tiba;


    ProgressDialog progressDialog;
    ProgressBar progressBar;
    CardView container_main;
    TextView lb_btn;

    DO data;

    TextView tv_title;
    TextView tv_jumlah_foto;
    EditText et_jumlah_barang;

    private final static int PERMISSION_LOAD_GALLERY = 1;
    private final static int PERMISSION_LOAD_CAMERA = 2;
    private final static int PERMISSION_CALL = 3;
    private static int PRESCRIPTION_LOAD_IMG = 1;
    private static int PRESCRIPTION_LOAD_CAM = 2;

    private GridView imagesGrid;
    private ImagesGridAdapter gridAdapter;
    private List<Bitmap> bitmaps;
    private List<String> paths;


    private String cameraImageUri;
    private String spk_number;
    private String reason;
    private String TAG="Detail angkut";

    private Boolean noManifestleft=false;
    private int qty=0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_angkut);

        getBundle();
        setToolbar();
        setContent();
        setNavigation();

        if(spk_number!=null) getData();

    }

    private void getBundle(){
        Bundle b= getIntent().getExtras();
        try {
            spk_number =b.getString("spk_number");
            noManifestleft = b.getBoolean("no_manifest_left");
        }catch (Throwable throwable){}
    }

    private void setContent(){
        progressBar=findViewById(R.id.progressBar);
        container_main=findViewById(R.id.container_main);

        tv_jumlah_foto=findViewById(R.id.tv_jumlah_foto);
        et_jumlah_barang=findViewById(R.id.et_jumlah_barang);
        lb_btn=findViewById(R.id.lb_btn);


        //image
        bitmaps = new ArrayList<>();
        paths = new ArrayList<>();
        bitmaps.add(null);
        gridAdapter = new ImagesGridAdapter(this, bitmaps,false);
        imagesGrid = (GridView) findViewById(R.id.prescription_images);
        if (imagesGrid != null) {
            imagesGrid.setAdapter(gridAdapter);
        }
    }

    private Boolean isArrived(){
        boolean result=true;
        if(data!=null){
            if(data.status.equalsIgnoreCase("selesai muat")) result = false;
            else result = true;
        }
        return result;
    }

    private Boolean isMulaiBongkar(){
        boolean result=false;
        if(data!=null){
            if(data.status.equalsIgnoreCase("mulai bongkar")) result = true;
            else result = false;
        }
        return result;
    }

    private void setNavigation(){
        container_jumlah_barang=(LinearLayout)findViewById(R.id.container_jumlah_barang);
        container_foto=(LinearLayout)findViewById(R.id.container_foto);
        btn_tiba=(CardView)findViewById(R.id.btn_tiba);

        btn_tiba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isArrived()) {
                    startUnLoading(spk_number);
                }else {
                    arrivalDestination(spk_number);
                }
            }
        });

        findViewById(R.id.btn_lihat_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(data!=null) {
                    Bundle b = new Bundle();
                    b.putSerializable("data", data);
                    Intent intent = new Intent(DetailAngkutActivity.this, MapActivity.class);
                    intent.putExtras(b);
                    startActivity(intent);
                }
            }
        });

        findViewById(R.id.btn_selesai).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!et_jumlah_barang.getText().toString().isEmpty()){

                    int jumlah=0;
                    try {
                        jumlah = Integer.parseInt(et_jumlah_barang.getText().toString());
                    }catch (Throwable throwable){}

                    if (bitmaps.size() > 1) {

                        if(jumlah==qty){
                            compressImage(false);

                        }else{
                            dialog_cancel_list(jumlah);


                        }

                    }else {
                        Toast.makeText(getApplicationContext(), "Silahkan upload gambar dahulu", Toast.LENGTH_LONG).show();
                    }


                }else{
                    Toast.makeText(getApplicationContext(),"Masukkan jumlah aktual barang",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void dialog_cancel_list(final int jumlah){
        DialogCancelOrder dial = new DialogCancelOrder(this,false, new DialogCancelOrder.ListenerDialog() {
            @Override
            public void onSelected(boolean canInput, String value) {
                if (canInput) {
                    reason=value;

                    if(jumlah==0){
                        compressImage(true);
                    }else{
                        compressImage(false);
                    }

                } else {
                    dialog_cancel_text(jumlah);
                }
            }
        });
        dial.show();
    }

    private void dialog_cancel_text(final int jumlah){
        DialogCancelText dial = new DialogCancelText(this, new DialogCancelText.ListenerDialog() {
            @Override
            public void onSelected(String value) {
                reason=value;

                if(jumlah==0){
                    compressImage(true);
                }else{
                    compressImage(false);
                }

            }
        });
        dial.show();
    }



    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        container_main.setVisibility(View.GONE);
    }

    private void hideLoading(){
        progressBar.setVisibility(View.GONE);
        container_main.setVisibility(View.VISIBLE);
    }

    /**
     * menampilkan progress dialog
     */
    protected void showProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(this);
            }
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }catch (Throwable th){}
    }

    /**
     * sembunyikan progress dialog
     */
    protected void dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }catch (Throwable te){}
    }


    @SuppressLint("SetTextI18n")
    private void setData(){
        TextView tv_status= findViewById(R.id.tv_status);
        TextView tv_date_from= findViewById(R.id.tv_date_from);
        TextView tv_year_from= findViewById(R.id.tv_year_from);
        TextView tv_date_to= findViewById(R.id.tv_date_to);
        TextView tv_year_to= findViewById(R.id.tv_year_to);
        TextView tv_location_from= findViewById(R.id.tv_location_from);
        TextView tv_name_from= findViewById(R.id.tv_corporate_from);
        TextView tv_location_to= findViewById(R.id.tv_location_to);
        TextView tv_name_to= findViewById(R.id.tv_corporate_to);
        TextView tv_qty= findViewById(R.id.tv_qty);

        if(data!=null){

            tv_title.setText(data.do_number);

            if(data.status!=null){
                String upperString = data.status.substring(0, 1).toUpperCase() + data.status.substring(1);
                tv_status.setText("*"+upperString);

            }


            tv_location_from.setText(data.origin_area);
            tv_name_from.setText(data.origin_name +"\n"+data.origin_address);
            tv_location_to.setText(data.destination_area);
            tv_name_to.setText(data.destination_name +"\n"+data.destination_address);


            if (data.req_pick_up_date != null && !data.req_pick_up_date.equals("")) {
                tv_date_from.setText(customConvertDate(data.req_pick_up_date));
                tv_year_from.setText(customConvertDateYear(data.req_pick_up_date));
            }

            if (data.req_delivery_date != null && !data.req_delivery_date.equals("")) {
                tv_date_to.setText(customConvertDate(data.req_delivery_date));
                tv_year_to.setText(customConvertDateYear(data.req_delivery_date));
            }



            qty=0;
            try {
                qty = Integer.parseInt(data.qty);
            }catch (Throwable throwable){}


            tv_qty.setText("Jumlah unit barang : "+qty);

            if(isArrived()){
                lb_btn.setText("MULAI BONGKAR");
            }else{
                lb_btn.setText("TIBA LOKASI BONGKAR");
            }

            if(isMulaiBongkar()){
                container_jumlah_barang.setVisibility(View.VISIBLE);
                container_foto.setVisibility(View.VISIBLE);
                btn_tiba.setVisibility(View.GONE);
            }else{
                container_jumlah_barang.setVisibility(View.GONE);
                container_foto.setVisibility(View.GONE);
                btn_tiba.setVisibility(View.VISIBLE);
            }

        }
    }


    private void getData(){
        showLoading();
        API.doDetail(this, spk_number, new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                hideLoading();
                try {
                    JSONObject obj=new JSONObject(response);
                    if(httpCode==200) {

                        data = new DO(obj);


                        setData();
                    }else{
                        String message=obj.getString("message");
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    hideLoading();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                hideLoading();
            }
        });
    }

    private void arrivalDestination(String id){
        showProgressDialog();
        API.arrivalDestination(this,id,  new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                dismissProgressDialog();
                try {

                    JSONObject data=new JSONObject(response);
                    if(httpCode==200) {

                        String id = data.getString("spk_number");


                        getData();

                        if(noManifestleft) {

                            stopTrackingService();

                        }


                    }else{
                        dismissProgressDialog();

                        String message=data.getString("message");
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    dismissProgressDialog();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                dismissProgressDialog();
            }
        });
    }



    private void startUnLoading(String id){
        showProgressDialog();
        API.startUnLoading(this,id,  new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                dismissProgressDialog();

                try {

                    JSONObject data=new JSONObject(response);
                    if(httpCode==200) {

                        String id = data.getString("spk_number");

                        getData();



                    }else{
                        String message=data.getString("message");
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    dismissProgressDialog();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                dismissProgressDialog();
            }
        });
    }


    private void finishUnLoading(String id,String actual_qty, String reason,ArrayList<File> myFiles){
        showProgressDialog();


        API.finishUnLoading(this, id, actual_qty, reason, myFiles,  new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                dismissProgressDialog();

                try {

                    JSONObject data=new JSONObject(response);
                    if(httpCode==200) {

                        String id = data.getString("spk_number");

//                        Bundle b =new Bundle();
//                        b.putInt("page",2);
//                        Intent intent = new Intent(DetailAngkutActivity.this, HomeActivity.class);
//                        intent.putExtras(b);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(intent);
//                        finish();

                        Intent intent = new Intent(DetailAngkutActivity.this, FinishTripActivity.class);
                        startActivity(intent);

                    }else{
                        String message=data.getString("message");
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    dismissProgressDialog();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                dismissProgressDialog();
            }
        });
    }

    private void failedUnLoading(String id,String reason ,ArrayList<File> myFiles){
        showProgressDialog();


        API.failedUnLoading(this, id, reason,  myFiles, new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                dismissProgressDialog();

                try {

                    JSONObject data=new JSONObject(response);
                    if(httpCode==200) {

                        String id = data.getString("spk_number");

                        Bundle b =new Bundle();
                        b.putInt("page",2);
                        Intent intent = new Intent(DetailAngkutActivity.this, HomeActivity.class);
                        intent.putExtras(b);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();


                    }else{
                        String message=data.getString("message");
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    dismissProgressDialog();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                dismissProgressDialog();
            }
        });
    }




    private void setToolbar(){
        RelativeLayout btn_back=findViewById(R.id.btn_back);
        RelativeLayout btn_info=findViewById(R.id.btn_info);
        tv_title=findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);



        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_info.setVisibility(View.VISIBLE);
        btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionCallPhone();
            }
        });
        bottomNav();
    }

    public void permissionCallPhone(){
        if (ContextCompat.checkSelfPermission(mActivity, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity, new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_CALL);
        }
        else {
            alertCall();
        }

    }


    private void bottomNav(){
        findViewById(R.id.btn_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogout();
            }
        });

        findViewById(R.id.btn_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goHome();
            }
        });
    }



    public String customConvertDate(String currentDate){
        String date = currentDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }

    public String customConvertDateYear(String currentDate){
        String date = currentDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }


    void compressImage(Boolean isFailed) {
        ArrayList<File> myImages=new ArrayList<File>();
        boolean pass = true;
        if (paths.size() <= 0) pass = false;
        if (pass) {
            //process items
            String dirPath = String.valueOf(getExternalFilesDir(null));

            for (String path : paths) {
                try {
                    // Update the protocol
                    if (!path.matches("^(http|file|content|assets|drawable):.+$")) {
                        path = "file://" + path;
                    }
                    //get the image and resize
                    Bitmap bm;
                    bm = ImageLoader.getInstance().loadImageSync(path, new ImageSize(800, 800));

                    if (bm != null) {
                        //save to compressed folder
                        String filename = "compressed_" + System.currentTimeMillis() + ".jpg";
                        String compressedPath = dirPath + File.separator + "compressed" + File.separator + filename;

                        File comp = new File(compressedPath);
                        File dir = new File(comp.getParent());

                        if (comp.getParent() != null && !dir.isDirectory()) {
                            dir.mkdirs();
                        }

                        FileOutputStream fos = new FileOutputStream(comp);
                        bm.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                        fos.flush();
                        fos.close();

                        //recycle for other use
                        bm.recycle();
                        myImages.add(comp);
                    } else {
                        Toast.makeText(getApplicationContext(),"gagal proses gambar",Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),"kesalahan proses gambar",Toast.LENGTH_LONG).show();
                }
            }

            if (myImages.size() > 0) {

                if(isFailed){
                    failedUnLoading(spk_number,reason, myImages);
                }else {
                    finishUnLoading(spk_number, et_jumlah_barang.getText().toString(), reason, myImages);
                }

            } else {

                Toast.makeText(getApplicationContext(),"daftar gambar kosong",Toast.LENGTH_LONG).show();

            }
        } else {
            Toast.makeText(getApplicationContext(),"Pastikan anda sudah memilih foto",Toast.LENGTH_LONG).show();

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                File imageFile = null;
                if (cameraImageUri != null) imageFile = new File(cameraImageUri);
//                if not respecting the output_extra, we take it manually from DCIM
                if (data != null
                        || (imageFile != null && imageFile.exists())
                        ) {
                    if (data != null
                            && data.getExtras() != null
                            && data.getExtras().get("data") != null &&
                            data.getData() != null
                            ) {
                        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                        String uri = data.getData().toString();
                        addPreview(thumbnail, uri);
                    }
                    //if brought to cameraImageUri, we create thumbnail ourselves
                    else {
                        if (requestCode == PRESCRIPTION_LOAD_CAM) {
                            int SIZE_THUMB = 200;
                            Bitmap bm;

                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(cameraImageUri, options);

                            int scale = 1;
                            while (options.outWidth / scale / 2 >= SIZE_THUMB && options.outHeight / scale / 2 >= SIZE_THUMB)
                                scale *= 2;
                            options.inSampleSize = scale;
                            options.inJustDecodeBounds = false;
                            bm = BitmapFactory.decodeFile(cameraImageUri, options);
                            addPreview(bm, cameraImageUri);

                        } else {
                            String uri = data != null ? data.getDataString() : null;
                            if (uri != null) {
                                addPreview(ImageLoader.getInstance().loadImageSync(uri), uri);
                            }
                        }
                    }


                } else {
                    Toast.makeText(getApplicationContext(),"belum memilih foto ",Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(getApplicationContext(),"kesalahan proses gambar",Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) e.printStackTrace();
            Toast.makeText(getApplicationContext(),"kesahalan pengambilan foto ",Toast.LENGTH_LONG).show();

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (cameraImageUri != null) {
            outState.putString("cameraImageUri", cameraImageUri);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("cameraImageUri")) {
            cameraImageUri = savedInstanceState.getString("cameraImageUri");
        }
    }

    //create preview image

    private void addPreview(Bitmap bitmap, String path) {
        if (bitmap != null) {
            bitmaps.add(bitmaps.size()-1 , bitmap);
            paths.add(path);
            updateGrid();
        }
    }

    //delete item in bitmaps
    public void removePrescriptionImage(int position) {
        bitmaps.remove(position);
        paths.remove(position);
        updateGrid();
    }

    //update preview images
    public void updateGrid() {
        if (bitmaps.size() == 1) {
            imagesGrid.setVisibility(View.VISIBLE);
        }
        tv_jumlah_foto.setText("Jumlah foto POD : " + (bitmaps.size()-1)+"/2");
        gridAdapter.notifyDataSetChanged();
        imagesGrid.invalidateViews();
    }

    //open dialog
    public void openDialog() {
        // custom dialog
        final Context context = this;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_upload);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });

        LinearLayout llCamera = (LinearLayout) dialog.findViewById(R.id.llCamera);
        LinearLayout llGallery = (LinearLayout) dialog.findViewById(R.id.llGallery);
        // if button is clicked, close the custom dialog
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeImage();
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageFromGallery();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    void takeImage() {
        String dirPath = String.valueOf(getExternalFilesDir(null));
        if (dirPath != null) {
            if (Build.VERSION.SDK_INT >= 23) {
                int hasCameraPermission = ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA);
                if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA}, PERMISSION_LOAD_CAMERA);
                    return;
                }
            }

            loadCamera();

        } else {
            Toast.makeText(getApplicationContext(),"Terjadi kesalahan pemrosesan gambar, mohon mencoba kembali",Toast.LENGTH_LONG).show();
        }
    }

    void getImageFromGallery() {

        // Check permissions
        if (Build.VERSION.SDK_INT >= 23) {
            int hasWriteStoragePermission = ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_LOAD_GALLERY);
                return;
            }
        }

        loadImageFromGallery();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_LOAD_GALLERY: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    loadImageFromGallery();

                } else {
                    Toast.makeText(getApplicationContext(),"Gagal mengakses Galeri" ,Toast.LENGTH_LONG).show();
                }
            }

            case PERMISSION_LOAD_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadCamera();
                } else {
                    Toast.makeText(getApplicationContext(),"Gagal mengakses kamera" ,Toast.LENGTH_LONG).show();

                }
            }

            case PERMISSION_CALL:{
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    alertCall();
                } else {
                    Toast.makeText(getApplicationContext(),"Gagal mengakses telepon" ,Toast.LENGTH_LONG).show();

                }
            }
        }
    }

    private void loadImageFromGallery() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");

        // Start the Intent
        startActivityForResult(Intent.createChooser(galleryIntent, "Pilih Foto Resep"), PRESCRIPTION_LOAD_IMG);
    }

    private void loadCameraOld() {
        try {
            String dirpath = String.valueOf(getExternalFilesDir(null));

            String filename = "prescription_" + System.currentTimeMillis() + ".jpg";
            cameraImageUri = dirpath + File.separator + "camera" + File.separator + filename;
            //make folder if not exists
            File temp = new File(cameraImageUri);
            temp.getParentFile().mkdirs();

            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(temp));
            startActivityForResult(cameraIntent, PRESCRIPTION_LOAD_CAM);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            Uri photoURI = null;
            try {
//                File photoFile = createImageFileWith();
//                path = photoFile.getAbsolutePath();

                String dirpath = String.valueOf(getExternalFilesDir(null));

                String filename = "prescription_" + System.currentTimeMillis() + ".jpg";
                cameraImageUri = dirpath + File.separator + "camera" + File.separator + filename;
                //make folder if not exists
                File temp = new File(cameraImageUri);
                temp.getParentFile().mkdirs();


                photoURI = FileProvider.getUriForFile(DetailAngkutActivity.this,
                        getString(R.string.file_provider_authority),
                        temp);

            } catch (Exception ex) {
                Log.e("TakePicture", ex.getMessage());
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(takePictureIntent, PRESCRIPTION_LOAD_CAM);
        }
    }




    /////////////////////////////////// service tracking /////////////////////////////////////


    private FusedLocationProviderClient mFusedLocationClient;

    private void stopTrackingService(){
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        removeLocationUpdates();
    }

    private PendingIntent getPendingIntent() {
        // Note: for apps targeting API level 25 ("Nougat") or lower, either
        // PendingIntent.getService() or PendingIntent.getBroadcast() may be used when requesting
        // location updates. For apps targeting API level O, only
        // PendingIntent.getBroadcast() should be used. This is due to the limits placed on services
        // started in the background in "O".

        // TODO(developer): uncomment to use PendingIntent.getService().
//        Intent intent = new Intent(this, LocationUpdatesIntentService.class);
//        intent.setAction(LocationUpdatesIntentService.ACTION_PROCESS_UPDATES);
//        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intent = new Intent(this, LocationUpdatesBroadcastReceiver.class);
        intent.setAction(LocationUpdatesBroadcastReceiver.ACTION_PROCESS_UPDATES);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    /**
     * Handles the Remove Updates button, and requests removal of location updates.
     */
    public void removeLocationUpdates() {
        Log.i(TAG, "Removing location updates");
        Utils.setRequestingLocationUpdates(this, false);
        mFusedLocationClient.removeLocationUpdates(getPendingIntent());
    }


}
