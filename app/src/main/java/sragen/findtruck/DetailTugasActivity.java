package sragen.findtruck;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sragen.findtruck.adapter.ImagesGridAdapter;
import sragen.findtruck.adapter.TugasChildAdapter;
import sragen.findtruck.app.API;
import sragen.findtruck.app.ErrorCallback;
import sragen.findtruck.app.ResponseCallback;
import sragen.findtruck.model.Manifest;
import sragen.findtruck.utilities.BaseActivity;
import sragen.findtruck.utilities.DialogCancelOrder;
import sragen.findtruck.utilities.DialogCancelText;

/**
 * Created by adikurniawan on 07/05/18.
 */

public class DetailTugasActivity extends BaseActivity {

    LinearLayout container_foto;
    CardView btn_mulai_muat;
    String id_manifest;
    TextView tv_title;

    ProgressDialog progressDialog;
    ProgressBar progressBar;
    CardView container_main;

    Manifest data;

    TextView tv_jumlah_foto;
    EditText et_jumlah_barang;


    private final static int PERMISSION_LOAD_GALLERY = 1;
    private final static int PERMISSION_LOAD_CAMERA = 2;
    private final static int PERMISSION_CALL= 3;
    private static int PRESCRIPTION_LOAD_IMG = 1;
    private static int PRESCRIPTION_LOAD_CAM = 2;

    private GridView imagesGrid;
    private ImagesGridAdapter gridAdapter;
    private List<Bitmap> bitmaps;
    private List<String> paths;


    private String cameraImageUri;
    private int qty=0;
    private String reason;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tugas);

        getBundle();
        setToolbar();
        setContent();
        setNavigation();

        if(id_manifest!=null) getData();
    }

    private void getBundle(){
        Bundle b= getIntent().getExtras();
        try {
            id_manifest =b.getString("id_manifest");
        }catch (Throwable throwable){}
    }

    private void setContent(){
        progressBar=findViewById(R.id.progressBar);
        container_main=findViewById(R.id.container_main);

        tv_jumlah_foto=findViewById(R.id.tv_jumlah_foto);
        et_jumlah_barang=findViewById(R.id.et_jumlah_barang);

        //image
        bitmaps = new ArrayList<>();
        paths = new ArrayList<>();
        bitmaps.add(null);
        gridAdapter = new ImagesGridAdapter(this, bitmaps,true);
        imagesGrid = (GridView) findViewById(R.id.prescription_images);
        if (imagesGrid != null) {
            imagesGrid.setAdapter(gridAdapter);
        }

    }

    @SuppressLint("SetTextI18n")
    private void setData(){
        TextView tv_status=findViewById(R.id.batch);
        TextView tv_date=findViewById(R.id.tv_date);
        TextView tv_pengiriman=findViewById(R.id.tv_pengiriman);
        TextView tv_vehicle_id=findViewById(R.id.tv_vehicle_id);
        TextView tv_vehicle_type=findViewById(R.id.tv_vehicle_type);
        TextView tv_from=findViewById(R.id.tv_from);
        TextView tv_destination=findViewById(R.id.tv_destination);
        TextView tv_client_name=findViewById(R.id.tv_client_name);
        RecyclerView rv=findViewById(R.id.rv);
        TextView tv_qty=findViewById(R.id.tv_qty);


        if(data!=null){
            tv_title.setText(data.id_manifest);

            tv_from.setText(data.origin_area);
            tv_destination.setText(data.destination_area);
            tv_client_name.setText(data.client_name);
            tv_pengiriman.setText(data.dispatch_order.size()+" Pengiriman ");
            tv_vehicle_id.setText(data.vehicle_id);
            tv_vehicle_type.setText(data.vehicle_type);

            if(data.status_manifest!=null) {
                String upperString = data.status_manifest.substring(0, 1).toUpperCase() + data.status_manifest.substring(1);
                tv_status.setText("* " + upperString);

                if(data.status_manifest.equalsIgnoreCase("tiba lokasi muat")) {
                    container_foto.setVisibility(View.GONE);
                    btn_mulai_muat.setVisibility(View.VISIBLE);
                }else{
                    container_foto.setVisibility(View.VISIBLE);
                    btn_mulai_muat.setVisibility(View.GONE);
                }


            }

            if(data.delivery_date!=null && !data.delivery_date.equals("")){
                tv_date.setText(customConvertDate(data.delivery_date));
            }else{
                tv_date.setText("");
            }


            qty=0;
            if(data.dispatch_order.size()>0){
                TugasChildAdapter adapter = new TugasChildAdapter(this,data.dispatch_order);
                rv.setLayoutManager(new LinearLayoutManager(rv.getContext()));
                rv.setAdapter(adapter);

                for(int i=0;i<data.dispatch_order.size();i++){
                    if(!data.dispatch_order.get(i).qty.equals("")) {
                        qty = Integer.parseInt(data.dispatch_order.get(i).qty) + qty;
                    }
                }
            }
            tv_qty.setText("Jumlah unit barang : "+qty);
        }


    }

    private void setNavigation(){
        container_foto=(LinearLayout)findViewById(R.id.container_foto);
        btn_mulai_muat=(CardView)findViewById(R.id.btn_mulai_muat);

        findViewById(R.id.btn_selesai).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!et_jumlah_barang.getText().toString().isEmpty()){
                    if(et_jumlah_barang.getText().toString().equals("0")){

                        if (bitmaps.size() > 1) {
                            dialog_cancel_list();

                        } else {
                            Toast.makeText(getApplicationContext(), "Silahkan upload gambar dahulu", Toast.LENGTH_LONG).show();
                        }

                    }
                    else if(et_jumlah_barang.getText().toString().equals(qty+"")) {
                        if (bitmaps.size() > 1) {
                            compressImage(false);
                        } else {
                            Toast.makeText(getApplicationContext(), "Silahkan upload gambar dahulu", Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Jumlah barang tidak sama", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Masukkan jumlah aktual barang",Toast.LENGTH_LONG).show();
                }
            }
        });
        btn_mulai_muat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(id_manifest!=null) startLoading(id_manifest);
            }
        });
    }


    private void dialog_cancel_list(){
        DialogCancelOrder dial = new DialogCancelOrder(this, true, new DialogCancelOrder.ListenerDialog() {
            @Override
            public void onSelected(boolean canInput, String value) {
                if (canInput) {
                    reason=value;
                    compressImage(true);
                } else {
                    dialog_cancel_text();
                }
            }
        });
        dial.show();
    }

    private void dialog_cancel_text(){
        DialogCancelText dial = new DialogCancelText(this, new DialogCancelText.ListenerDialog() {
            @Override
            public void onSelected(String value) {
                reason=value;
                compressImage(true);
            }
        });
        dial.show();
    }



    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        container_main.setVisibility(View.GONE);
    }

    private void hideLoading(){
        progressBar.setVisibility(View.GONE);
        container_main.setVisibility(View.VISIBLE);
    }

    /**
     * menampilkan progress dialog
     */
    protected void showProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(this);
            }
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }catch (Throwable th){}
    }

    /**
     * sembunyikan progress dialog
     */
    protected void dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }catch (Throwable te){}
    }



    private void getData(){
        showLoading();
        API.getDispatchedDetail(this, id_manifest, new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                hideLoading();
                try {
                    JSONObject obj=new JSONObject(response);
                    if(httpCode==200) {

                        data = new Manifest(obj);


                        setData();
                    }else{
                        String message=obj.getString("message");
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    hideLoading();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                hideLoading();
            }
        });
    }

    private void startLoading(String id){
        showProgressDialog();
        API.startLoading(this,id,  new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                dismissProgressDialog();

                try {

                    JSONObject data=new JSONObject(response);
                    if(httpCode==200) {

                        String id = data.getString("id_manifest");

                        getData();

                    }else{
                        String message=data.getString("message");
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    dismissProgressDialog();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                dismissProgressDialog();
            }
        });
    }


    private void finishLoading(String id,String qty, ArrayList<File> myFiles){
        showProgressDialog();


        API.finishLoading(this, id, qty, myFiles,  new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                dismissProgressDialog();

                try {

                    JSONObject data_=new JSONObject(response);
                    if(httpCode==200) {

                        String id = data_.getString("id_manifest");

                        Bundle b=new Bundle();
                        b.putSerializable("id_manifest",data.id_manifest);
                        Intent intent = new Intent(DetailTugasActivity.this, StartTripActivity.class);
                        intent.putExtras(b);
                        startActivity(intent);


                    }else{
                        String message=data_.getString("message");
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    dismissProgressDialog();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                dismissProgressDialog();
            }
        });
    }

    private void failedLoading(String id,String reason, ArrayList<File> myFiles){
        showProgressDialog();


        API.failedLoading(this, id, reason, myFiles, new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                dismissProgressDialog();

                try {

                    JSONObject data=new JSONObject(response);
                    if(httpCode==200) {

                        String id = data.getString("id_manifest");

                        finish();


                    }else{
                        String message=data.getString("message");
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    dismissProgressDialog();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                dismissProgressDialog();
            }
        });
    }


    private void setToolbar(){
        RelativeLayout btn_back=findViewById(R.id.btn_back);
        RelativeLayout btn_info=findViewById(R.id.btn_info);
        tv_title=findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_info.setVisibility(View.VISIBLE);
        btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionCallPhone();
            }
        });
        bottomNav();
    }

    public void permissionCallPhone(){
        if (ContextCompat.checkSelfPermission(mActivity, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity, new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_CALL);
        }
        else {
            alertCall();
        }

    }


    private void bottomNav(){
        findViewById(R.id.btn_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogout();
            }
        });

        findViewById(R.id.btn_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goHome();
            }
        });
    }



    public String customConvertDate(String currentDate){
        String date = currentDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }

    void compressImage(Boolean isFailed) {
        ArrayList<File> myImages=new ArrayList<File>();
        boolean pass = true;
        if (paths.size() <= 0) pass = false;
        if (pass) {
            //process items
            String dirPath = String.valueOf(getExternalFilesDir(null));

            for (String path : paths) {
                try {
                    // Update the protocol
                    if (!path.matches("^(http|file|content|assets|drawable):.+$")) {
                        path = "file://" + path;
                    }
                    //get the image and resize
                    Bitmap bm;
                    bm = ImageLoader.getInstance().loadImageSync(path, new ImageSize(800, 800));

                    if (bm != null) {
                        //save to compressed folder
                        String filename = "compressed_" + System.currentTimeMillis() + ".jpg";
                        String compressedPath = dirPath + File.separator + "compressed" + File.separator + filename;

                        File comp = new File(compressedPath);
                        File dir = new File(comp.getParent());

                        if (comp.getParent() != null && !dir.isDirectory()) {
                            dir.mkdirs();
                        }

                        FileOutputStream fos = new FileOutputStream(comp);
                        bm.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                        fos.flush();
                        fos.close();

                        //recycle for other use
                        bm.recycle();
                        myImages.add(comp);
                    } else {
                        Toast.makeText(getApplicationContext(),"gagal proses gambar",Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),"kesalahan proses gambar",Toast.LENGTH_LONG).show();
                }
            }

            if (myImages.size() > 0) {

                if(isFailed){
                    failedLoading(id_manifest,reason, myImages);

                }else{
                    finishLoading(id_manifest,et_jumlah_barang.getText().toString(),myImages);
                }

            } else {

                Toast.makeText(getApplicationContext(),"daftar gambar kosong",Toast.LENGTH_LONG).show();

            }
        } else {
            Toast.makeText(getApplicationContext(),"Pastikan anda sudah memilih foto",Toast.LENGTH_LONG).show();

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                File imageFile = null;
                if (cameraImageUri != null) imageFile = new File(cameraImageUri);
//                if not respecting the output_extra, we take it manually from DCIM
                if (data != null
                        || (imageFile != null && imageFile.exists())
                        ) {
                    if (data != null
                            && data.getExtras() != null
                            && data.getExtras().get("data") != null &&
                            data.getData() != null
                            ) {
                        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                        String uri = data.getData().toString();
                        addPreview(thumbnail, uri);
                    }
                    //if brought to cameraImageUri, we create thumbnail ourselves
                    else {
                        if (requestCode == PRESCRIPTION_LOAD_CAM) {
                            int SIZE_THUMB = 200;
                            Bitmap bm;

                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(cameraImageUri, options);

                            int scale = 1;
                            while (options.outWidth / scale / 2 >= SIZE_THUMB && options.outHeight / scale / 2 >= SIZE_THUMB)
                                scale *= 2;
                            options.inSampleSize = scale;
                            options.inJustDecodeBounds = false;
                            bm = BitmapFactory.decodeFile(cameraImageUri, options);
                            addPreview(bm, cameraImageUri);

                        } else {
                            String uri = data != null ? data.getDataString() : null;
                            if (uri != null) {
                                addPreview(ImageLoader.getInstance().loadImageSync(uri), uri);
                            }
                        }
                    }


                } else {
                    Toast.makeText(getApplicationContext(),"belum memilih foto ",Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(getApplicationContext(),"kesalahan proses gambar",Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) e.printStackTrace();
            Toast.makeText(getApplicationContext(),"kesahalan pengambilan foto ",Toast.LENGTH_LONG).show();

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (cameraImageUri != null) {
            outState.putString("cameraImageUri", cameraImageUri);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("cameraImageUri")) {
            cameraImageUri = savedInstanceState.getString("cameraImageUri");
        }
    }

    //create preview image

    private void addPreview(Bitmap bitmap, String path) {
        if (bitmap != null) {
            bitmaps.add(bitmaps.size()-1 , bitmap);
            paths.add(path);
            updateGrid();
        }
    }

    //delete item in bitmaps
    public void removePrescriptionImage(int position) {
        bitmaps.remove(position);
        paths.remove(position);
        updateGrid();
    }

    //update preview images
    public void updateGrid() {
        if (bitmaps.size() == 1) {
            imagesGrid.setVisibility(View.VISIBLE);
        }
        tv_jumlah_foto.setText("Jumlah foto POD : " + (bitmaps.size()-1)+"/2");
        gridAdapter.notifyDataSetChanged();
        imagesGrid.invalidateViews();
    }

    //open dialog
    public void openDialog() {
        // custom dialog
        final Context context = this;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_upload);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });

        LinearLayout llCamera = (LinearLayout) dialog.findViewById(R.id.llCamera);
        LinearLayout llGallery = (LinearLayout) dialog.findViewById(R.id.llGallery);
        // if button is clicked, close the custom dialog
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeImage();
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageFromGallery();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    void takeImage() {
        String dirPath = String.valueOf(getExternalFilesDir(null));
        if (dirPath != null) {
            if (Build.VERSION.SDK_INT >= 23) {
                int hasCameraPermission = ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA);
                if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA}, PERMISSION_LOAD_CAMERA);
                    return;
                }
            }

            loadCamera();

        } else {
            Toast.makeText(getApplicationContext(),"Terjadi kesalahan pemrosesan gambar, mohon mencoba kembali",Toast.LENGTH_LONG).show();
        }
    }

    void getImageFromGallery() {

        // Check permissions
        if (Build.VERSION.SDK_INT >= 23) {
            int hasWriteStoragePermission = ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_LOAD_GALLERY);
                return;
            }
        }

        loadImageFromGallery();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_LOAD_GALLERY: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    loadImageFromGallery();

                } else {
                    Toast.makeText(getApplicationContext(),"Gagal mengakses Galeri" ,Toast.LENGTH_LONG).show();
                }
                break;
            }

            case PERMISSION_LOAD_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadCamera();
                } else {
                    Toast.makeText(getApplicationContext(),"Gagal mengakses kamera" ,Toast.LENGTH_LONG).show();

                }
                break;
            }

            case PERMISSION_CALL:{
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    alertCall();
                } else {
                    Toast.makeText(getApplicationContext(),"Gagal mengakses telepon" ,Toast.LENGTH_LONG).show();

                }
                break;

            }
        }
    }

    private void loadImageFromGallery() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");

        // Start the Intent
        startActivityForResult(Intent.createChooser(galleryIntent, "Pilih Foto Resep"), PRESCRIPTION_LOAD_IMG);
    }

    private void loadCameraOld() {
        try {
            String dirpath = String.valueOf(getExternalFilesDir(null));

            String filename = "prescription_" + System.currentTimeMillis() + ".jpg";
            cameraImageUri = dirpath + File.separator + "camera" + File.separator + filename;
            //make folder if not exists
            File temp = new File(cameraImageUri);
            temp.getParentFile().mkdirs();

            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(temp));
            startActivityForResult(cameraIntent, PRESCRIPTION_LOAD_CAM);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            try {

                Uri photoURI = null;
//                File photoFile = createImageFileWith();
//                path = photoFile.getAbsolutePath();

                String dirpath = String.valueOf(getExternalFilesDir(null));

                String filename = "prescription_" + System.currentTimeMillis() + ".jpg";
                cameraImageUri = dirpath + File.separator + "camera" + File.separator + filename;
                //make folder if not exists
                File temp = new File(cameraImageUri);
                temp.getParentFile().mkdirs();


                photoURI = FileProvider.getUriForFile(DetailTugasActivity.this,
                        getString(R.string.file_provider_authority),
                        temp);


            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(takePictureIntent, PRESCRIPTION_LOAD_CAM);

            } catch (Exception ex) {
                Log.e("TakePicture", ex.getMessage());
            }
        }
    }


}

