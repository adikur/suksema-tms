package sragen.findtruck.app;

/**
 * Created by adikurniawan on 06/05/18.
 */

public interface ResponseCallback {
    int doAction(String response, int httpCode);
}
