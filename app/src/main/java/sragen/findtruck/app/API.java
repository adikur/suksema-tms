package sragen.findtruck.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import sragen.findtruck.BuildConfig;
import sragen.findtruck.R;
import sragen.findtruck.model.Location;
import sragen.findtruck.model.Trip;
import sragen.findtruck.model.User;
import sragen.findtruck.utilities.SharedPreference;

/**
 * Created by adikurniawan on 06/05/18.
 */

public class API {



//    public static String APIHost = "http://stms.goodevamedia.com/index.php/Api/";

    public static String APIHost = "https://www.suksematms.co.id/taco/index.php/Api/";
    public static String APITracking = "https://www.suksematms.co.id/taco/";

//    public static String APIHost = "http://bic.suksematms.co.id/index.php/Api/";


    public static String my_token = "";

    public static void login(Context context, String username, String password, ResponseCallback onSuccess, ErrorCallback onError) {

        SharedPreferences pref = TruckApp.getAppContext().getSharedPreferences(ConfigNotif.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        String url;
        url = APIHost + "Auth/login";

        Map<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        params.put("push_notification_id", regId);
        RequestManager.post(context, url, params, onSuccess, onError);
    }

    public static void logout(Context context,ResponseCallback onSuccess, ErrorCallback onError) {
        String url;
        url = APIHost + "Auth/logout";

        Map<String, String> params = new HashMap<>();
        RequestManager.post(context, url, params, onSuccess, onError);
    }

    public static void getDispatched(Context context, ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/dispatched";
        RequestManager.get(context, url, onSuccess, onError);
    }

    public static void getDispatchedDetail(Context context,String id,  ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/manifestDetail/"+id;
        RequestManager.get(context, url, onSuccess, onError);
    }

    public static void arrivalOrigin(Context context,String manifest_id,  ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/arrivalOrigin";

        Map<String, String> params = new HashMap<>();
        params.put("manifest_id", manifest_id);
        RequestManager.post(context, url, params, onSuccess, onError);
    }


    public static void startLoading(Context context,String manifest_id,  ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/startLoading";

        Map<String, String> params = new HashMap<>();
        params.put("manifest_id", manifest_id);
        RequestManager.post(context, url, params, onSuccess, onError);
    }

    public static void finishLoading(Context context, String manifest_id,String qty, ArrayList<File> images, ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/finishLoading";

        Map<String, String> params = new HashMap<>();
        params.put("manifest_id", manifest_id);
        params.put("actual_qty", qty);

        RequestManager.postUpload(context, url, params, images, onSuccess, onError);
    }

    public static void failedLoading(Context context,String manifest_id,String reason, ArrayList<File> images,  ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/failedLoading";

        Map<String, String> params = new HashMap<>();
        params.put("manifest_id", manifest_id);
        params.put("reason", reason);
        RequestManager.postUpload(context, url, params, images, onSuccess, onError);

    }


    public static void getPickup(Context context, ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/pickup";
        RequestManager.get(context, url, onSuccess, onError);
    }

    public static void doDetail(Context context,String id,  ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/doDetail/"+id;
        RequestManager.get(context, url, onSuccess, onError);
    }

    public static void arrivalDestination(Context context,String spk_number,  ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/arrivalDestination";

        Map<String, String> params = new HashMap<>();
        params.put("spk_number", spk_number);
        RequestManager.post(context, url, params, onSuccess, onError);
    }

    public static void startUnLoading(Context context,String spk_number,  ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/startUnLoading";

        Map<String, String> params = new HashMap<>();
        params.put("spk_number", spk_number);
        RequestManager.post(context, url, params, onSuccess, onError);
    }

    public static void finishUnLoading(Context context, String spk_number, String qty, String reason,  ArrayList<File> images, ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/finishUnLoading";

        Map<String, String> params = new HashMap<>();
        params.put("spk_number", spk_number);
        params.put("actual_qty", qty);
        if(reason!=null){
            params.put("reason", reason);
        }

        RequestManager.postUpload(context, url, params, images, onSuccess, onError);
    }

    public static void failedUnLoading(Context context,String spk_number,String reason, ArrayList<File> images,  ResponseCallback onSuccess, ErrorCallback onError) {
        String url = APIHost + "Order/failedUnloading";

        Map<String, String> params = new HashMap<>();
        params.put("spk_number", spk_number);
        params.put("reason", reason);
        RequestManager.postUpload(context, url, params, images, onSuccess, onError);
    }


    public static void getHistory(Context context,String date,  ResponseCallback onSuccess, ErrorCallback onError) {
        String url;
        if(date!=null) {
            url = APIHost + "Order/history?date=" + date;
        }else{
            url = APIHost + "Order/history";
        }
        RequestManager.get(context, url, onSuccess, onError);
    }




    public static void sendLocation(Context context, Double lat, Double lng ,  String current_time,  ResponseCallback onSuccess, ErrorCallback onError) {

        Context ctx = TruckApp.getAppContext();

        Trip trip=null;
        try {
            trip = (Trip) SharedPreference.Get(ctx.getApplicationContext(), ctx.getApplicationContext().getString(R.string.my_trip), Trip.class);
        }catch (Throwable throwable){
            Log.e("API","API Trip "+throwable.toString());
        }

        String url;
        url = APITracking+ "updatelocation.php" +
                "?latitude="+lat+"" +
                "&longitude="+lng+"" +
                "&gpstime="+ current_time ;
            if(trip!=null){
                url +="&username=" +trip.username +
                        "&sessionid=" +trip.spk_number +
                        "&phonenumber=" +trip.phone ;
            }

        RequestManager.get(context, url, onSuccess, onError);
    }





    public static class RequestManager {
        private final static OkHttpClient httpClient = new OkHttpClient();

        private static String stringifyRequestBody(Request request) {
            try {
                okio.Buffer buffer = new okio.Buffer();
                request.newBuilder().build().body().writeTo(buffer);

                return buffer.readUtf8();
            } catch (IOException e) {

            }

            return null;
        }

        private static Request getRequest(String url, RequestBody requestBody) {
            Headers.Builder httpHeadersBuilder = new Headers.Builder();
            String token = getToken();

            httpHeadersBuilder.add("Accept", "application/json");

            if (!token.equals("")) {
                httpHeadersBuilder.add("X-Authorization", "Bearer " + token);
            }


            Headers httpHeaders = httpHeadersBuilder.build();
            Request.Builder requestBuilder = new Request.Builder()
                    .url(url)
                    .headers(httpHeaders);


            if (requestBody != null) {
                requestBuilder.post(requestBody);
            }

            Request request = requestBuilder.build();

            String requestBodyString = null;
            if (requestBody != null) requestBodyString = stringifyRequestBody(request);

            if (BuildConfig.DEBUG) {
                Log.d("API", "API URL: " + url);
                Log.d("API", "API Headers Sent: " + httpHeaders.toString());
                if (requestBody != null)
                    Log.d("API", "API POST : " + (requestBodyString != null ? requestBodyString : "NULL"));
            }
            return request;
        }


        private static void createHttpRequest(Context context, Request request,
                                              final ResponseCallback onSuccess, final ErrorCallback onError) {

          /*  httpClient.setCookieHandler(new CookieManager(
                    new PersistentCookieStore(context),
                    CookiePolicy.ACCEPT_ALL));
*/

            httpClient.setConnectTimeout(60, TimeUnit.SECONDS);
            httpClient.setReadTimeout(60, TimeUnit.SECONDS);

            httpClient.newCall(request).enqueue(new Callback() {
                Handler mainHandler = new Handler(Looper.getMainLooper());

                @Override
                public void onFailure(Request request, IOException e) {
                    if (BuildConfig.DEBUG) e.printStackTrace();


                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (onError != null) {
                                onError.doAction();
                            }
                        }
                    });
                }

                @Override
                public void onResponse(final Response response) throws IOException {
                    final String responseBody = response.body().string();
                    final int responseCode = response.code();

                    if (BuildConfig.DEBUG) {
                        Log.d("API", "API Response Code: " + responseCode);
                        Log.d("API", "API Response: " + responseBody);
                    }

                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (onSuccess != null) {
                                onSuccess.doAction(responseBody, responseCode);
                            }
                        }
                    });
                }
            });
        }

        public static void get(Context context, final String url,
                               final ResponseCallback onSuccess, final ErrorCallback onError) {

            Request request = getRequest(url, null);
            createHttpRequest(context, request, onSuccess, onError);
        }

        public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        public static void post_json(Context context, final String url, String json,
                                     final ResponseCallback onSuccess, final ErrorCallback onError) {

            Log.d("API", "API Media type : " + JSON);
            RequestBody body = RequestBody.create(JSON, json.getBytes());
            Request request = getRequest(url, body);

            createHttpRequest(context, request, onSuccess, onError);
        }

        public static void post(Context context, final String url, final Map<String, String> parameters,
                                final ResponseCallback onSuccess, final ErrorCallback onError) {

            FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();

            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                if (entry.getKey() != null && entry.getValue() != null) {
                    formEncodingBuilder.add(entry.getKey(), entry.getValue());
                }
            }

            RequestBody requestBody = formEncodingBuilder.build();
            Request request = getRequest(url, requestBody);

            createHttpRequest(context, request, onSuccess, onError);
        }

        public static void postUpload(Context context, final String url,
                                      final Map<String, String> parameters, final ArrayList<File> attachments,
                                      final ResponseCallback onSuccess, final ErrorCallback onError) {

            MultipartBuilder multipartBuilder = new MultipartBuilder().type(MultipartBuilder.FORM);

            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                try {
                    multipartBuilder.addFormDataPart(entry.getKey(), entry.getValue());
                }catch (Throwable th){}
            }

            int i=1;
            for (File file : attachments) {
                multipartBuilder.addFormDataPart(
                        "photo_"+i,
                        file.getName(),
                        RequestBody.create(
                                MediaType.parse(getMimeType(file.getPath())),
                                file
                        )
                );
                i++;
            }

            RequestBody requestBody = multipartBuilder.build();
            Request request = getRequest(url, requestBody);

            createHttpRequest(context, request, onSuccess, onError);
        }

        //mime type
        public static String getMimeType(String url) {
            String type = null;
            String extension = MimeTypeMap.getFileExtensionFromUrl(url);
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
            return type;
        }

        public static String getIMEI() {
            String imei = "";
            try {
                Context ctx = TruckApp.getAppContext();

                String deviceId = "";
                deviceId = Settings.Secure.getString(ctx.getContentResolver(),
                        Settings.Secure.ANDROID_ID);

                if (!deviceId.equals("")) imei = deviceId;
                else imei = "12345678";

            } catch (Throwable e) {
                e.printStackTrace();
            }
            return imei;
        }

        public static String getToken() {
            String token = "";
            Context ctx = TruckApp.getAppContext();


            User user=null;
            try {
                user = (User) SharedPreference.Get(ctx.getApplicationContext(), ctx.getApplicationContext().getString(R.string.data_user), User.class);
            }catch (Throwable throwable){
                Log.e("API","API Token "+throwable.toString());
            }


            if (user!=null) {
                token = user.token;
            }
            Log.d("API", "API Token "+token);


            return token;
        }


    }

}
