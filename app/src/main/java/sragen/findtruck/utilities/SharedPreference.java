package sragen.findtruck.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.Type;

import sragen.findtruck.R;

/**
 * Created by adikurniawan on 23/04/18.
 */

public class SharedPreference {
    public static void Save(Context context, String key, Object value){
        SharedPreferences sharepref = context.getSharedPreferences(context.getString(R.string.shared_preference_key), Context.MODE_PRIVATE);
        sharepref.edit().putString(key, new Gson().toJson(value)).apply();
    }

    public static void Clear(Context context, String key){
        SharedPreferences sharepref = context.getSharedPreferences(context.getString(R.string.shared_preference_key), Context.MODE_PRIVATE);
        sharepref.edit().remove(key).apply();
    }

    public static Object Get(Context context, String key, Class<?> type){
        SharedPreferences sharepref = context.getSharedPreferences(context.getString(R.string.shared_preference_key), Context.MODE_PRIVATE);
        //save to shared preference
        String logres = sharepref.getString(key, "");

        if(!logres.isEmpty()){
            return new Gson().fromJson(logres, type);
        }

        return null;
    }

    public static Object Get(Context context, String key, Type type){
        SharedPreferences sharepref = context.getSharedPreferences(context.getString(R.string.shared_preference_key), Context.MODE_PRIVATE);
        //save to shared preference
        String logres = sharepref.getString(key, "");

        if(!logres.isEmpty()){
            return new Gson().fromJson(logres, type);
        }

        return null;
    }
}