package sragen.findtruck.utilities;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import sragen.findtruck.app.TruckApp;

public class Utilities {

    //check network connection
    public static boolean isOnline() {
        ConnectivityManager connectivity = (ConnectivityManager) TruckApp.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//        return (networkInfo != null && networkInfo.isConnected());
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public static void scheduleNotification(String trackingLabel, Notification notification, int delay, int scheduleId, int notificationId, int pendingIntentFlag) {
        Context c = TruckApp.getAppContext();

        Intent notificationIntent = new Intent(c, LocalNotificationReceiver.class);

        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION, notification);
        notificationIntent.putExtra(LocalNotificationReceiver.TRACKING_LABEL, trackingLabel);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(c, scheduleId, notificationIntent, pendingIntentFlag);

        long futureInMillis = System.currentTimeMillis() + delay;
        AlarmManager alarmManager = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);
    }

    public static void cancelScheduledNotification(int scheduleId) {
        Context c = TruckApp.getAppContext();

        Intent intent = new Intent(c, LocalNotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(c, scheduleId, intent, 0);

        if (pendingIntent != null) {
            AlarmManager alarmManager = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
        }
    }
}
