package sragen.findtruck.utilities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.internal.Util;
import sragen.findtruck.HomeActivity;
import sragen.findtruck.LoginActivity;
import sragen.findtruck.R;
import sragen.findtruck.StartTripActivity;
import sragen.findtruck.app.API;
import sragen.findtruck.app.ErrorCallback;
import sragen.findtruck.app.ResponseCallback;
import sragen.findtruck.model.User;
import sragen.findtruck.services.AndroidLocationServices;
import sragen.findtruck.services.MyAlarmService;

public class BaseActivity extends AppCompatActivity {

    private static int REQUEST_PHONE_CALL =122;

    private static Map<Class<? extends Activity>, Activity> launched = new HashMap<Class<? extends Activity>, Activity>();
    protected Activity mActivity;
    protected ProgressDialog progressDialog;
    protected User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity activity = launched.get(this.getClass());
        if (activity != null)
            activity.finish();


        launched.put(this.getClass(), this);
        mActivity=this;
    }

    public void alertCall(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(mActivity, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(mActivity);
        }
        builder.setTitle("")
                .setMessage("Lakukan Panggilan atau Chat whatsapp ?")
                .setPositiveButton("Panggilan", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        call();
                    }
                })
                .setNegativeButton("Chat Whatsapp", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        chatWhatsApp();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }



    public void call(){
        String number_phone = getString(R.string.phone_cs);
        number_phone = number_phone.replace(" ", "");
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number_phone + ""));
        if (ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    @SuppressLint("NewApi")
    public void chatWhatsApp() {
        String formattedNumber = getString(R.string.phone_cs);
        try{
            Intent sendIntent =new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT,"");
            sendIntent.putExtra("jid", formattedNumber +"@s.whatsapp.net");
            sendIntent.setPackage("com.whatsapp");
            startActivity(sendIntent);
        }
        catch(Exception e)
        {
            Toast.makeText(mActivity,"Error/n"+ e.toString(),Toast.LENGTH_SHORT).show();
        }
    }

    public void callWhatsapp(){
        ContentResolver resolver = mActivity.getContentResolver();
        Cursor cursor = resolver.query(
                ContactsContract.Data.CONTENT_URI,
                null, null, null,
                ContactsContract.Contacts.DISPLAY_NAME);

        long _id= 0;
        while (cursor.moveToNext()) {
            _id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Data._ID));
            String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
            String mimeType = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.MIMETYPE));

            Log.d("Data", _id+ " "+ displayName + " " + mimeType );
        }
        try {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse("content://com.android.contacts/data/" + _id),
                    "vnd.android.cursor.item/vnd.com.whatsapp.voip.call");
            intent.setPackage("com.whatsapp");

            startActivity(intent);
        }catch (Throwable throwable){
        }

    }



    public void goHome(){
        Intent intent = new Intent(mActivity, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void doLogout(){
        showProgressDialog();
        API.logout(this, new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                dismissProgressDialog();

                try {
                    JSONObject responseObj=new JSONObject(response);

                    if(httpCode==200 || httpCode==401) {
                        // remove data user //
                        SharedPreference.Clear(mActivity,getString(R.string.data_user));

                        // stop service location //
                        //stopService(new Intent(mActivity, AndroidLocationServices.class));

                        // stop service alarm //
                        Intent intent = new Intent(mActivity, MyAlarmService.class);
                        PendingIntent pintent = PendingIntent.getService(mActivity, 0, intent, 0);
                        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                        alarm.cancel(pintent);



                        cekLogin();

                    }else{
                        String message=responseObj.getString("message");
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    dismissProgressDialog();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                dismissProgressDialog();
            }
        });
    }

    private void cekLogin(){
        try{
            user=(User) SharedPreference.Get(getApplicationContext(),getString(R.string.data_user),User.class);
        }catch (Throwable throwable){}
        if(user==null){

            Intent intent = new Intent(mActivity, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();

        }
    }

    /**
     * menampilkan progress dialog
     */
    protected void showProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(this);
            }
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }catch (Throwable th){}
    }

    /**
     * sembunyikan progress dialog
     */
    protected void dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }catch (Throwable te){}
    }

}
