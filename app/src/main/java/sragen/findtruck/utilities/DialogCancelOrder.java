package sragen.findtruck.utilities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import sragen.findtruck.R;
import sragen.findtruck.adapter.AngkutAdapter;
import sragen.findtruck.adapter.ReasonAdapter;

@SuppressLint("DefaultLocale")
public class DialogCancelOrder extends Dialog implements View.OnClickListener {

    RecyclerView recyclerView;
    ListenerDialog listenerDialog;
    ReasonAdapter reasonAdapter;
    String[] text;




    public DialogCancelOrder(Context context, Boolean isTugas, ListenerDialog listener){
        super(context, R.style.TransparantDialogTheme);
        this.listenerDialog=listener;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.dialog_cancel_order);

        if(isTugas) {

            text = new String[]{
                    "Salah PO",
                    "Customer Membatalkan Order",
                    "Kemasan Barang Rusak",
                    "Jumlah Tidak Sesuai",
                    "Masalah Dokumen",
                    "Lain Lain"
            };

        }else {

            text = new String[]{
                    "Alamat Toko Tidak Jelas",
                    "Customer Membatalkan Order",
                    "Salah Type Barang",
                    "Kemasan Barang Rusak",
                    "Jumlah Tidak Sesuai",
                    "Masalah Dokumen",
                    "Toko Tutup",
                    "Driver Telat Sampai",
                    "Akses Jalan Tidak Bisa",
                    "Lain Lain"
            };

        }


        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);
        reasonAdapter = new ReasonAdapter(context, text, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(reasonAdapter);




    }

    public void onClicked(int pos,String value){
        if((pos+1)==text.length){
            Log.e("dialog",false+"");
            listenerDialog.onSelected(false,value);
        }else{
            Log.e("dialog",true+"");

            listenerDialog.onSelected(true,value);
        }
        dismiss();


    }

    public interface ListenerDialog{
        void onSelected(boolean isi,String value);
    }

    @Override
    public void onClick(View v) {

    }

}

