package sragen.findtruck.utilities;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import sragen.findtruck.R;

public class DialogResponse extends Dialog {

    public DialogResponse(Context context) {
        super(context);
        super.requestWindowFeature(Window.FEATURE_NO_TITLE);
        initDialog();
    }
    public DialogResponse(Context context, DialogType dialogType) {
        super(context);
        super.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialogType(dialogType);
        initDialog();
    }
    public DialogResponse(Context context, int themeResId) {
        super(context, themeResId);
    }

    public enum DialogType{
        INFO, YES_NO
    }

    DialogType dialogType = DialogType.YES_NO;
    Button yes, no;
    TextView message;

    /**
     * by default dialog type is YES OR NO
     * @param dialogType
     */
    public void setDialogType(DialogType dialogType) {
        this.dialogType = dialogType;
    }

    void initDialog(){
        this.setContentView(R.layout.component_dialog_prompt);
        yes = (Button)findViewById(R.id.btnYes);
        no = (Button)findViewById(R.id.btnNo);
        message = (TextView)findViewById(R.id.tvMessage);

        if(dialogType == DialogType.INFO){
            no.setVisibility(View.GONE);
        }
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DialogResponse.this.isShowing()) {
                    DialogResponse.this.dismiss();
                }
            }
        });
    }

    public void setYesText(String text) {
        this.yes.setText(text);
    }
    public void setNoText(String text) {
        this.no.setText(text);
    }

    public void setYesOnClickListener(View.OnClickListener o) {
        yes.setOnClickListener(o);
    }
    public void setNoOnClickListener(View.OnClickListener o) {
        no.setOnClickListener(o);
    }
    public void setMessage(int message) {
        setMessage(getContext().getString(message));
    }
    public void setMessage(String message) {
        this.message.setText(message);
    }
}
