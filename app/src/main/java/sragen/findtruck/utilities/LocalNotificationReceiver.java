package sragen.findtruck.utilities;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.HashMap;

public class LocalNotificationReceiver extends BroadcastReceiver {
    public static String NOTIFICATION_ID = "notificationID";
    public static String NOTIFICATION = "notification";
    public static String TRACKING_LABEL = "trackID";

    public void onReceive(Context context, Intent intent) {
        try {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Notification notification = intent.getParcelableExtra(NOTIFICATION);
            int notificationId = intent.getIntExtra(NOTIFICATION_ID, 0);
            String trackingLabel = intent.getStringExtra(TRACKING_LABEL);

            if (notification != null) {
                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                notification.defaults |= Notification.DEFAULT_VIBRATE;
                notification.defaults |= Notification.DEFAULT_SOUND;
            } {



                notificationManager.notify(notificationId, notification);
            }
        } catch (Exception e) {
        }
    }
}
