package sragen.findtruck.utilities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import sragen.findtruck.R;

@SuppressLint("DefaultLocale")
public class DialogStatus extends Dialog implements View.OnClickListener {

    ListenerDialog listenerDialog;
    TextView tv0, tv1, tv2;

    public DialogStatus(Context context, ListenerDialog listener){
        super(context, R.style.TransparantDialogTheme);
        this.listenerDialog=listener;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.dialog_status);

        tv0=(TextView) findViewById(R.id.tv0);
        tv1=(TextView) findViewById(R.id.tv1);
        tv2=(TextView) findViewById(R.id.tv2);


        tv0.setOnClickListener(this);
        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
    }

    public interface ListenerDialog{
        void onSelected(String value);
    }

    @Override
    public void onClick(View v) {
        if(v==tv0){
            dismiss();
            listenerDialog.onSelected(tv0.getText().toString());
        } else if(v==tv1){
            dismiss();
            listenerDialog.onSelected(tv1.getText().toString());
        }else if(v==tv2){
            dismiss();
            listenerDialog.onSelected(tv2.getText().toString());
        }
    }

}
