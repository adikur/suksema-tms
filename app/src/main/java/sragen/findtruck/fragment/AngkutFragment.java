package sragen.findtruck.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import sragen.findtruck.R;
import sragen.findtruck.adapter.AngkutAdapter;
import sragen.findtruck.adapter.AngkutAdapter;
import sragen.findtruck.app.API;
import sragen.findtruck.app.ErrorCallback;
import sragen.findtruck.app.ResponseCallback;
import sragen.findtruck.model.DO;
import sragen.findtruck.model.Manifest;

/**
 * Created by adikurniawan on 03/05/18.
 */

public class AngkutFragment extends Fragment {


    private AngkutAdapter angkutAdapter;
    private ArrayList<DO> dataList=new ArrayList<DO>();
    private RecyclerView recyclerView;

    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_angkut, container, false);
        

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        setContent(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        getList();

    }

    void onItemsLoadComplete() {

        swipeRefreshLayout.setRefreshing(false);
    }

    private void setContent(View view){

        recyclerView= view.findViewById(R.id.recyclerview);
        progressBar= view.findViewById(R.id.progressBar);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                getList();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.blueLight,
                R.color.colorPrimary,
                R.color.colorPrimaryDark);


        angkutAdapter=new AngkutAdapter(getActivity(),dataList);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(angkutAdapter);



    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    private void hideLoading(){
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    /**
     * menampilkan progress dialog
     */
    protected void showProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
            }
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }catch (Throwable th){}
    }

    /**
     * sembunyikan progress dialog
     */
    protected void dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }catch (Throwable te){}
    }



    private void getList(){
        showLoading();
        API.getPickup(getActivity(), new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                hideLoading();
                onItemsLoadComplete();
                try {

                    if(httpCode==200) {
                        dataList.clear();

                        JSONArray responseArr=new JSONArray(response);


                        for(int i=0;i<responseArr.length();i++){
                            JSONObject data=responseArr.getJSONObject(i);

                            dataList.add(new DO(data));
                        }

                        angkutAdapter.notifyDataSetChanged();


                    }



                } catch (JSONException e) {
                    hideLoading();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                hideLoading();
            }
        });
    }
}
