package sragen.findtruck.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import sragen.findtruck.R;
import sragen.findtruck.adapter.TerkirimAdapter;
import sragen.findtruck.adapter.TerkirimAdapter;
import sragen.findtruck.app.API;
import sragen.findtruck.app.ErrorCallback;
import sragen.findtruck.app.ResponseCallback;
import sragen.findtruck.model.DO;
import sragen.findtruck.utilities.DialogCancelOrder;
import sragen.findtruck.utilities.DialogStatus;

/**
 * Created by adikurniawan on 03/05/18.
 */

public class KirimFragment extends Fragment implements DatePickerDialog.OnDateSetListener {


    private TerkirimAdapter terkirimAdapter;
    private ArrayList<DO> dataList=new ArrayList<DO>();
    private RecyclerView recyclerView;

    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;

    private String filter_date;
    private String status;
    private TextView tv_status;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_kirim, container, false);

       


        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        
        setContent(view);

    }

    @Override
    public void onResume() {
        super.onResume();
        getList(null);


    }

    void onItemsLoadComplete() {

        swipeRefreshLayout.setRefreshing(false);
    }

    private void setContent(View view){

        recyclerView= view.findViewById(R.id.recyclerview);
        progressBar= view.findViewById(R.id.progressBar);
        tv_status=view.findViewById(R.id.tv_status);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                status=null;
                getList(null);
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.blueLight,
                R.color.colorPrimary,
                R.color.colorPrimaryDark);


        terkirimAdapter=new TerkirimAdapter(getActivity(),dataList);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(terkirimAdapter);


        view.findViewById(R.id.img_calendar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDate();
            }
        });
        tv_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_status();
            }
        });

    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    private void hideLoading(){
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    /**
     * menampilkan progress dialog
     */
    protected void showProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
            }
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }catch (Throwable th){}
    }

    /**
     * sembunyikan progress dialog
     */
    protected void dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }catch (Throwable te){}
    }



    private void getList(String date){
        showLoading();
        API.getHistory(getActivity(),date,  new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                hideLoading();
                onItemsLoadComplete();
                try {

                    if(httpCode==200) {
                        dataList.clear();

                        JSONArray responseArr=new JSONArray(response);


                        for(int i=0;i<responseArr.length();i++){
                            JSONObject data=responseArr.getJSONObject(i);

                            if(status!=null){
                                Log.e("status",status);
                                Log.e("status",data.getString("status"));
                                if(data.getString("status").equalsIgnoreCase(status)) {
                                    dataList.add(new DO(data));
                                }
                            }else{
                                dataList.add(new DO(data));
                            }

                        }

                        terkirimAdapter.notifyDataSetChanged();


                    }



                } catch (JSONException e) {
                    hideLoading();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                hideLoading();
            }
        });
    }

    private void dialog_status(){
        DialogStatus dial = new DialogStatus(getActivity(), new DialogStatus.ListenerDialog() {
            @Override
            public void onSelected(String value) {
                if(value.equals("semua status")){
                    status=null;
                }else{
                    status=value;
                }
                tv_status.setText(value);
                getList(null);
            }
        });
        dial.show();
    }



    public void callDate(){
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                KirimFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setVersion(DatePickerDialog.Version.VERSION_1);
        dpd.vibrate(true);
        dpd.dismissOnPause(true);
       /* if (modeCustomAccentDate.isChecked()) {
            dpd.setAccentColor(Color.parseColor("#9C27B0"));
        }
        if (highlightDays.isChecked()) {
            Calendar date1 = Calendar.getInstance();
            Calendar date2 = Calendar.getInstance();
            date2.add(Calendar.WEEK_OF_MONTH, -1);
            Calendar date3 = Calendar.getInstance();
            date3.add(Calendar.WEEK_OF_MONTH, 1);
            Calendar[] days = {date1, date2, date3};
            dpd.setHighlightedDays(days);
        }
        if (limitSelectableDays.isChecked()) {
            Calendar[] days = new Calendar[13];
            for (int i = -6; i < 7; i++) {
                Calendar day = Calendar.getInstance();
                day.add(Calendar.DAY_OF_MONTH, i * 2);
                days[i + 6] = day;
            }
            dpd.setSelectableDays(days);
        }*/
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: "+dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
        //Toast.makeText(getActivity(),date,Toast.LENGTH_LONG).show();

        filter_date=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        getList(filter_date);
    }

}
