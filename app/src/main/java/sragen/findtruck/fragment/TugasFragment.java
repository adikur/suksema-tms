package sragen.findtruck.fragment;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import sragen.findtruck.BuildConfig;
import sragen.findtruck.DetailTugasActivity;
import sragen.findtruck.HomeActivity;
import sragen.findtruck.LoginActivity;
import sragen.findtruck.R;
import sragen.findtruck.adapter.TugasAdapter;
import sragen.findtruck.app.API;
import sragen.findtruck.app.ErrorCallback;
import sragen.findtruck.app.ResponseCallback;
import sragen.findtruck.model.Manifest;
import sragen.findtruck.model.User;
import sragen.findtruck.services.AndroidLocationServices;
import sragen.findtruck.services.MyAlarmService;
import sragen.findtruck.utilities.SharedPreference;

/**
 * Created by adikurniawan on 03/05/18.
 */

public class TugasFragment extends Fragment {

    private TugasAdapter tugasAdapter;
    private ArrayList<Manifest> dataList=new ArrayList<Manifest>();
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;

    private SwipeRefreshLayout swipeRefreshLayout;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tugas, container, false);



        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setContent(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        getList();

    }

    void onItemsLoadComplete() {

        swipeRefreshLayout.setRefreshing(false);
    }

    private void setContent(View view){

        recyclerView= view.findViewById(R.id.recyclerview);
        progressBar= view.findViewById(R.id.progressBar);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                getList();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.blueLight,
                R.color.colorPrimary,
                R.color.colorPrimaryDark);


        tugasAdapter=new TugasAdapter(getActivity(),dataList,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(tugasAdapter);



    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    private void hideLoading(){
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    /**
     * menampilkan progress dialog
     */
    protected void showProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
            }
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }catch (Throwable th){}
    }

    /**
     * sembunyikan progress dialog
     */
    protected void dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }catch (Throwable te){}
    }



    private void getList(){
        showLoading();
        API.getDispatched(getActivity(), new ResponseCallback() {
                    @Override
                    public int doAction(String response, int httpCode) {
                        hideLoading();
                        onItemsLoadComplete();
                        try {

                            if(httpCode==401){
                                requiredLogin();

                            }

                            if(httpCode==200) {
                                dataList.clear();

                                JSONArray responseArr=new JSONArray(response);


                                for(int i=0;i<responseArr.length();i++){
                                    JSONObject data=responseArr.getJSONObject(i);

                                    dataList.add(new Manifest(data));
                                }

                                tugasAdapter.notifyDataSetChanged();


                            }



                        } catch (JSONException e) {
                            hideLoading();
                            e.printStackTrace();
                        }
                        return httpCode;
                    }
                }, new ErrorCallback() {
                    @Override
                    public void doAction() {
                        hideLoading();
                    }
                });
    }

    private void arrival(String id){
        showProgressDialog();
        API.arrivalOrigin(getActivity(),id,  new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                dismissProgressDialog();

                try {

                    if(httpCode==200) {

                        JSONObject data=new JSONObject(response);
                        String id = data.getString("id_manifest");

                        Bundle b=new Bundle();
                        b.putString("id_manifest",id);
                        Intent intent=new Intent(getActivity(),DetailTugasActivity.class);
                        intent.putExtras(b);
                        startActivity(intent);

                    }



                } catch (JSONException e) {
                    dismissProgressDialog();
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
                dismissProgressDialog();
            }
        });
    }


    public void dialog_confirm(String title,final String id){
        //try {
            final Context context = getActivity();
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_confirm);
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    if (dialog != null)
                        dialog.dismiss();
                }
            });
            dialog.setCancelable(false);

            TextView tv_title = (TextView) dialog.findViewById(R.id.tvTitle);
            TextView btnYes = (TextView) dialog.findViewById(R.id.btnYes);
            TextView btnNo = (TextView) dialog.findViewById(R.id.btnNo);

            if (title == null || title.equals("")) {
                tv_title.setVisibility(View.GONE);
            } else {
                tv_title.setVisibility(View.VISIBLE);
                tv_title.setText(title);
            }

            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                    arrival(id);
                }
            });

            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });





            dialog.show();
//        } catch (Exception e) {
//            if (BuildConfig.DEBUG) Log.e("error", "Cannot Show alertDialog");
//        }
    }

    private void requiredLogin(){
        // remove data user //
        SharedPreference.Clear(getActivity(),getString(R.string.data_user));

        // stop service location //
        //getActivity().stopService(new Intent(getActivity(), AndroidLocationServices.class));

        // stop service alarm //
        Intent intent = new Intent(getActivity(), MyAlarmService.class);
        PendingIntent pintent = PendingIntent.getService(getActivity(), 0, intent, 0);
        AlarmManager alarm = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pintent);


        User user=null;
        try{
            user=(User) SharedPreference.Get(getActivity().getApplicationContext(),getString(R.string.data_user),User.class);
        }catch (Throwable throwable){}
        if(user==null){

            Intent intent_ = new Intent(getActivity(), LoginActivity.class);
            intent_.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent_);
            getActivity().finish();

        }

    }
}
