package sragen.findtruck;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import sragen.findtruck.model.DO;
import sragen.findtruck.services.LocationUpdatesBroadcastReceiver;
import sragen.findtruck.utilities.BaseActivity;
import sragen.findtruck.utilities.GPSTracker;
import sragen.findtruck.utilities.Utils;

/**
 * Created by adikurniawan on 13/05/18.
 */

public class MapActivity extends BaseActivity implements OnMapReadyCallback, DirectionCallback {

    private static int PERMISSION_CALL=3;
    private GoogleMap map;
    private UiSettings mUiSettings;

    private LatLng MY_LOCATION = new LatLng(0, 0);
    private LatLng DESTINATION_LOCATION = new LatLng(0, 0);
    private LatLng TEMP_LOCATION = new LatLng(0, 0);
    private LatLng MONAS = new LatLng(-6.1756484, 106.8227754);
    private GPSTracker gps;

    private String TAG = "maps_activity";

    private TextView tv_title;
    private DO data;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        try {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.framentMap);
            mapFragment.getMapAsync(this);
        } catch (Throwable tr) {}

        setToolbar();
        getBundle();
        setContent();
    }

    private void getBundle(){
        Bundle b= getIntent().getExtras();
        try {
            data =(DO)b.getSerializable("data");
            tv_title.setText(data.do_number);
        }catch (Throwable throwable){}
    }

    private void setContent(){
        TextView tv_from=findViewById(R.id.tv_from);
        TextView tv_des=findViewById(R.id.tv_destination);

        if(data!=null){
            tv_from.setText("Lokasi Anda sekarang ");
            tv_des.setText(data.destination_area+"\n"+data.destination_name+"\n"+data.destination_address);


            if (MY_LOCATION.latitude == 0) getCurrentLocation();


            DESTINATION_LOCATION = new LatLng(data.destination_latitude, data.destination_longitude);
            Log.d(TAG, DESTINATION_LOCATION.toString());

            requestDirection();
        }
    }


    private void setToolbar(){
        RelativeLayout btn_back=findViewById(R.id.btn_back);
        RelativeLayout btn_info=findViewById(R.id.btn_info);
        tv_title=findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);



        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_info.setVisibility(View.VISIBLE);
        btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionCallPhone();
            }
        });
        bottomNav();
    }

    public void permissionCallPhone(){
        if (ContextCompat.checkSelfPermission(mActivity, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity, new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_CALL);
        }
        else {
            alertCall();
        }

    }


    private void bottomNav(){
        findViewById(R.id.btn_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogout();
            }
        });

        findViewById(R.id.btn_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goHome();
            }
        });
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (isMapPermissionGranted()) {
            setMap();
        }

        mUiSettings = map.getUiSettings();
        mUiSettings.setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


    }


    public boolean isMapPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasWriteContactsPermission == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    /**
     * handle response permission yang dilakukan oleh user
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(requestCode == PERMISSION_CALL){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                alertCall();
            } else {
                Toast.makeText(getApplicationContext(),"Gagal mengakses telepon" ,Toast.LENGTH_LONG).show();

            }
        }else {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                setMap();
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * setting map
     */
    private void setMap() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        if (MY_LOCATION.latitude == 0) getCurrentLocation();
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(MY_LOCATION, 11));
        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                //TODO: Any custom actions
                return false;
            }
        });

    }

    /**
     * get current location. Jika gagal mendapatkan current location. maka kordinat secara default akan diarahkan di monas
     */
    public void getCurrentLocation() {
        gps = new GPSTracker(this);
        if (gps.canGetLocation()) { // gps enabled
            MY_LOCATION = new LatLng(gps.getLatitude(), gps.getLongitude());
            // \n is for new line
            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
            MY_LOCATION = MONAS;
            //gps.stopUsingGPS();
        }
    }

    /**
     * request direction ke Google.
     * MY LOCATION = kordinat lokasi dokter sekarang
     * DESTINATION LOCATION = kordinat lokasi pasien
     */
    public void requestDirection() {
        Log.d(TAG, "Current : " + MY_LOCATION.toString());
        Log.d(TAG, "Destinat : " + DESTINATION_LOCATION.toString());
        GoogleDirection.withServerKey(getResources().getString(R.string.google_maps_key))
                .from(MY_LOCATION)
                .to(DESTINATION_LOCATION)
                .transportMode(TransportMode.DRIVING)
                .execute(this);
    }

    /**
     * response direction dari Google.
     * menampilkan garis direction dari lokasi dokter ke lokasi pasien
     * memabuat pin marker lokasi dokter dan lokasi pasien
     * membuat animation map
     *
     * @param direction
     * @param rawBody
     */
    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        if (direction.isOK()) {
            //map.addMarker(new MarkerOptions().position(MY_LOCATION).icon(BitmapDescriptorFactory.fromResource(R.mipmap.address)));
            map.addMarker(new MarkerOptions().position(DESTINATION_LOCATION).icon(BitmapDescriptorFactory.fromResource(R.mipmap.address)));


            ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
            map.addPolyline(DirectionConverter.createPolyline(MapActivity.this, directionPositionList, 6, Color.parseColor("#327DC0")));

//            LatLngBounds CENTER_LOCATION = new LatLngBounds(CURRENT_lOCATION, DESTINATION_lOCATION);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(MY_LOCATION, 15));
            map.animateCamera(CameraUpdateFactory.zoomIn());
            map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(DESTINATION_LOCATION)      // Sets the center of the map to Mountain View
                    .zoom(13)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else {
            Toast.makeText(getApplicationContext(), "direction gagal, silahkan coba lagi", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * menampilkan log ketika direction map gagal
     *
     * @param t
     */
    @Override
    public void onDirectionFailure(Throwable t) {
        Log.e("direction", t.getMessage());
    }




}
