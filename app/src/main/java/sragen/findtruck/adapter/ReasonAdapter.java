package sragen.findtruck.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import sragen.findtruck.R;
import sragen.findtruck.fragment.TugasFragment;
import sragen.findtruck.utilities.DialogCancelOrder;

public class ReasonAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder> {

    private Context context;
    private String[] dataList;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;


    private DialogCancelOrder dialog;


    public ReasonAdapter(Context context, String[] dataList, DialogCancelOrder dialog){
        this.context=context;
        this.dataList=dataList;
        this.dialog = dialog;
    }


    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == TYPE_ITEM) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_reason, viewGroup, false);

            return new ReasonAdapter.VHItem(itemView);
        } else if (i == TYPE_HEADER){
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.header_list_tugas, viewGroup, false);

            return new ReasonAdapter.VHHeader(itemView);
        }
        throw new RuntimeException("there is no type that matches the type " + i + " + make sure your using types correctly");

    }

    class VHItem extends RecyclerView.ViewHolder  {
        public TextView tv_reason;
        public LinearLayout btn_click;


        public VHItem(View itemView) {
            super(itemView);

            tv_reason=itemView.findViewById(R.id.tv1);
            btn_click=itemView.findViewById(R.id.btn_click);
        }



    }





    class VHHeader extends RecyclerView.ViewHolder {
        public VHHeader(View itemView) {
            super(itemView);
        }
    }



    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder variableViewHolder, final int i) {
        if (variableViewHolder instanceof ReasonAdapter.VHItem) {


            ReasonAdapter.VHItem holder = (ReasonAdapter.VHItem) variableViewHolder;





            holder.tv_reason.setText(dataList[i].toString());

            holder.btn_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    dialog.onClicked(i, dataList[i].toString());
                }
            });

        } else if (variableViewHolder instanceof ReasonAdapter.VHHeader) {

        }

    }



    @Override
    public int getItemCount() {
        return dataList.length;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_ITEM;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }




}