package sragen.findtruck.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import sragen.findtruck.DetailAngkutActivity;
import sragen.findtruck.DetailTugasActivity;
import sragen.findtruck.R;
import sragen.findtruck.utilities.DialogResponse;
import sragen.findtruck.utilities.SquareImageView;

class ImagesGridHolder {
    ImageView image;
    ImageView btn;

    ImagesGridHolder(View base){
        this.image = (ImageView) base.findViewById(R.id.prescription_comp_image);
        this.btn = (ImageView) base.findViewById(R.id.prescription_comp_remove);
    }
}

public class ImagesGridAdapter extends BaseAdapter {
    private Context context;
    private List<Bitmap> images;

    private Boolean isTugas;
    // Constructor
    public ImagesGridAdapter(Context context, List<Bitmap> bitmaps, Boolean isTugas) {

        this.context = context;
        this.images = bitmaps;
        this.isTugas= isTugas;
    }

    public int getCount() {
        return images.size();
    }

    public Bitmap getItem(int position) {
        return images.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return images.get(position) != null ? 1 : 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ImagesGridHolder holder;

        if (getItemViewType(position) == 1) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_images, null);
                holder = new ImagesGridHolder(convertView);
                convertView.setTag(holder);

            } else {
                holder = (ImagesGridHolder) convertView.getTag();
            }

            holder.image.setImageBitmap(images.get(position));
            holder.btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final DialogResponse dialog = new DialogResponse(context);
                    dialog.setMessage("Apakah anda yakin untuk membuang foto ini?");

                    dialog.setYesOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(isTugas) {
                                ((DetailTugasActivity) context).removePrescriptionImage(position);
                            }else{
                                ((DetailAngkutActivity) context).removePrescriptionImage(position);
                            }
                            dialog.dismiss();
                        }
                    });
                    dialog.setNoOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });
        } else {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_add_images, null);
            }
            RelativeLayout rlAdd = (RelativeLayout)convertView.findViewById(R.id.rlAddPrescriptionPhoto);
            rlAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isTugas){
                        ((DetailTugasActivity)context).openDialog();
                    }else{
                        ((DetailAngkutActivity)context).openDialog();
                    }
                }
            });
        }
        return convertView;
    }
}
