package sragen.findtruck.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.vision.text.Line;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import sragen.findtruck.DetailAngkutActivity;
import sragen.findtruck.DetailTugasActivity;
import sragen.findtruck.MapActivity;
import sragen.findtruck.R;
import sragen.findtruck.model.DO;

/**
 * Created by adikurniawan on 06/05/18.
 */

public class AngkutAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<DO> dataList=new ArrayList<DO>();

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    public AngkutAdapter(Context context, ArrayList<DO>dataList){
        this.context=context;
        this.dataList=dataList;
    }

    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == TYPE_ITEM) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_list_angkut, viewGroup, false);

            return new AngkutAdapter.VHItem(itemView);
        } else if (i == TYPE_HEADER){
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.header_list_tugas, viewGroup, false);

            return new AngkutAdapter.VHHeader(itemView);
        }
        throw new RuntimeException("there is no type that matches the type " + i + " + make sure your using types correctly");

    }

    class VHItem extends RecyclerView.ViewHolder {

        CardView cv;
        LinearLayout maps;
        TextView tv_spk_number;
        TextView tv_status;
        TextView tv_date_from;
        TextView tv_year_from;
        TextView tv_date_to;
        TextView tv_year_to;
        TextView tv_location_from;
        TextView tv_name_from;
        TextView tv_location_to;
        TextView tv_name_to;

        public VHItem(View itemView) {
            super(itemView);

            cv =(CardView)itemView.findViewById(R.id.cv);
            maps = (LinearLayout)itemView.findViewById(R.id.btn_lihat_map);
            tv_spk_number=itemView.findViewById(R.id.tv_spk_number);
            tv_status=itemView.findViewById(R.id.tv_status);
            tv_date_from=itemView.findViewById(R.id.tv_date_from);
            tv_year_from=itemView.findViewById(R.id.tv_year_from);
            tv_date_to=itemView.findViewById(R.id.tv_date_to);
            tv_year_to=itemView.findViewById(R.id.tv_year_to);
            tv_location_from=itemView.findViewById(R.id.tv_location_from);
            tv_name_from=itemView.findViewById(R.id.tv_corporate_from);
            tv_location_to=itemView.findViewById(R.id.tv_location_to);
            tv_name_to=itemView.findViewById(R.id.tv_corporate_to);
        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
        public VHHeader(View itemView) {
            super(itemView);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder variableViewHolder, int i) {

        if (variableViewHolder instanceof AngkutAdapter.VHItem) {


            AngkutAdapter.VHItem holder = (AngkutAdapter.VHItem) variableViewHolder;
            final DO data=dataList.get(i);
            if(data!=null) {
                holder.tv_spk_number.setText(data.spk_number+" - "+data.do_number);
                if(data.status!=null){
                    String upperString = data.status.substring(0, 1).toUpperCase() + data.status.substring(1);
                    holder.tv_status.setText("*"+upperString);
                }


                holder.tv_location_from.setText(data.origin_area);
                holder.tv_name_from.setText(data.origin_name +"\n"+data.origin_address);
                holder.tv_location_to.setText(data.destination_area);
                holder.tv_name_to.setText(data.destination_name +"\n"+data.destination_address);


                if (data.req_pick_up_date != null && !data.req_pick_up_date.equals("")) {
                    holder.tv_date_from.setText(customConvertDate(data.req_pick_up_date));
                    holder.tv_year_from.setText(customConvertDateYear(data.req_pick_up_date));
                }

                if (data.req_delivery_date != null && !data.req_delivery_date.equals("")) {
                    holder.tv_date_to.setText(customConvertDate(data.req_delivery_date));
                    holder.tv_year_to.setText(customConvertDateYear(data.req_delivery_date));
                }
            }

            holder.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /// ceking the last trip or no ///
                    boolean no_manifest_left;
                    if(dataList.size()==1) no_manifest_left=true;
                    else no_manifest_left =false;
                    ////////////////////////////////

                    Bundle b=new Bundle();
                    b.putString("spk_number",data.spk_number);
                    b.putBoolean("no_manifest_left",no_manifest_left);
                    Intent intent=new Intent(context,DetailAngkutActivity.class);
                    intent.putExtras(b);
                    context.startActivity(intent);

                }
            });
            holder.maps.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Bundle b=new Bundle();
                    b.putSerializable("data",data);
                    Intent intent=new Intent(context,MapActivity.class);
                    intent.putExtras(b);
                    context.startActivity(intent);

                }
            });


        } else if (variableViewHolder instanceof AngkutAdapter.VHHeader) {

        }

    }



    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_ITEM;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }




    public String customConvertDate(String currentDate){
        String date = currentDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }

    public String customConvertDateYear(String currentDate){
        String date = currentDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }


}