package sragen.findtruck.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import sragen.findtruck.DetailTugasActivity;
import sragen.findtruck.R;
import sragen.findtruck.fragment.TugasFragment;
import sragen.findtruck.model.Manifest;

/**
 * Created by adikurniawan on 03/05/18.
 */

public class TugasAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<Manifest> dataList=new ArrayList<Manifest>();
    private TugasFragment fr;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    public TugasAdapter(Context context, ArrayList<Manifest> dataList, TugasFragment tg){
        this.context=context;
        this.dataList=dataList;
        this.fr=tg;
    }



    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == TYPE_ITEM) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_list_tugas, viewGroup, false);

            return new VHItem(itemView);
        } else if (i == TYPE_HEADER){
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.header_list_tugas, viewGroup, false);

            return new VHHeader(itemView);
        }
        throw new RuntimeException("there is no type that matches the type " + i + " + make sure your using types correctly");

    }

    class VHItem extends RecyclerView.ViewHolder {
        public CardView cv;
        public CardView btn_tiba;
        public TextView tv_batch;
        public TextView tv_date;
        public TextView tv_pengiriman;
        public TextView tv_vehicle_id;
        public TextView tv_vehicle_type;
        public TextView tv_from;
        public TextView tv_client_name;
        public TextView tv_destination;
        public TextView label_button;
        public RecyclerView rv;


        public VHItem(View itemView) {
            super(itemView);
            cv=(CardView)itemView.findViewById(R.id.cv);
            btn_tiba=(CardView)itemView.findViewById(R.id.btn_tiba);

            tv_batch=itemView.findViewById(R.id.batch);
            tv_date=itemView.findViewById(R.id.date);
            tv_pengiriman=itemView.findViewById(R.id.pengiriman);
            tv_vehicle_id=itemView.findViewById(R.id.vehicle_id);
            tv_vehicle_type=itemView.findViewById(R.id.vehicle_type);
            tv_from=itemView.findViewById(R.id.tv_from);
            tv_client_name=itemView.findViewById(R.id.tv_client_name);
            tv_destination=itemView.findViewById(R.id.tv_destination);
            label_button=itemView.findViewById(R.id.label_button);
            rv=itemView.findViewById(R.id.rv);

        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
        public VHHeader(View itemView) {
            super(itemView);
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder variableViewHolder, int i) {

        if (variableViewHolder instanceof TugasAdapter.VHItem) {

            TugasAdapter.VHItem holder = (TugasAdapter.VHItem) variableViewHolder;
            final Manifest data=dataList.get(i-1);
            if(data!=null){
                holder.tv_from.setText(data.origin_area);
                holder.tv_destination.setText(data.destination_area);
                holder.tv_client_name.setText(data.client_name);
                holder.tv_pengiriman.setText(data.dispatch_order.size()+" Pengiriman ");
                holder.tv_vehicle_id.setText(data.vehicle_id);
                holder.tv_vehicle_type.setText(data.vehicle_type);

                holder.tv_batch.setText("* Milkrun");

                if(data.delivery_date!=null && !data.delivery_date.equals("")){
                    holder.tv_date.setText(customConvertDate(data.delivery_date));
                }else{
                    holder.tv_date.setText("");
                }

                if(data.dispatch_order.size()>0){
                    TugasChildAdapter adapter=new TugasChildAdapter(context,data.dispatch_order);
                    holder.rv.setLayoutManager(new LinearLayoutManager(holder.rv.getContext()));
                    holder.rv.setAdapter(adapter);
                }
            }

            if(data.status_manifest.equals("")) {
                holder.label_button.setText("TIBA LOKASI MUAT");
            }else{
                holder.label_button.setText("ON PROSES");
            }


            holder.btn_tiba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(data.status_manifest.equals("")){

                    String message = "Anda yakin SEMUA (DO) sudah tiba di lokasi muat ?";
                    fr.dialog_confirm(message, data.id_manifest);

                }else{

                    Bundle b=new Bundle();
                    b.putString("id_manifest",data.id_manifest);
                    Intent intent=new Intent(context,DetailTugasActivity.class);
                    intent.putExtras(b);
                    context.startActivity(intent);

                }

                }
            });

        } else if (variableViewHolder instanceof TugasAdapter.VHHeader) {

        }

    }



    @Override
    public int getItemCount() {
        return dataList.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }




    public String customConvertDate(String currentDate){
        String date = currentDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }


}