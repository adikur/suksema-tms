package sragen.findtruck.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import sragen.findtruck.R;
import sragen.findtruck.fragment.TugasFragment;
import sragen.findtruck.model.Manifest;

public class TugasChildAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<Manifest.Order> dataList=new ArrayList<Manifest.Order>();
    private TugasFragment fr;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    public TugasChildAdapter(Context context, ArrayList<Manifest.Order> dataList){
        this.context=context;
        this.dataList=dataList;
    }



    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == TYPE_ITEM) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_child_tugas, viewGroup, false);

            return new TugasChildAdapter.VHItem(itemView);
        } else if (i == TYPE_HEADER){
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.header_list_tugas, viewGroup, false);

            return new TugasChildAdapter.VHHeader(itemView);
        }
        throw new RuntimeException("there is no type that matches the type " + i + " + make sure your using types correctly");

    }

    class VHItem extends RecyclerView.ViewHolder {

        public TextView tv_destination_area;
        public TextView tv_spk_number;
        public TextView tv_status;


        public VHItem(View itemView) {
            super(itemView);

            tv_destination_area=itemView.findViewById(R.id.tv_destination_area);
            tv_status=itemView.findViewById(R.id.tv_status);
            tv_spk_number=itemView.findViewById(R.id.tv_spk_number);

        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
        public VHHeader(View itemView) {
            super(itemView);
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder variableViewHolder, int i) {

        if (variableViewHolder instanceof TugasChildAdapter.VHItem) {

            TugasChildAdapter.VHItem holder = (TugasChildAdapter.VHItem) variableViewHolder;
            final Manifest.Order data=dataList.get(i);
            if(data!=null){
                if(data.status!=null) {
                    String upperString = data.status.substring(0,1).toUpperCase() + data.status.substring(1);
                    holder.tv_status.setText(upperString);

                    if(data.status.equalsIgnoreCase("belum pick up")){
                        holder.tv_status.setTextColor(Color.parseColor("#E40B18"));
                    }else{
                        holder.tv_status.setTextColor(Color.parseColor("#20C450"));
                    }
                }
                holder.tv_spk_number.setText(data.do_number);
                holder.tv_destination_area.setText(data.destination_area);

            }

        } else if (variableViewHolder instanceof TugasChildAdapter.VHHeader) {

        }

    }



    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }






}