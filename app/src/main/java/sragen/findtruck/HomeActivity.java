package sragen.findtruck;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import sragen.findtruck.app.API;
import sragen.findtruck.app.ConfigNotif;
import sragen.findtruck.app.ErrorCallback;
import sragen.findtruck.app.ResponseCallback;
import sragen.findtruck.fragment.AngkutFragment;
import sragen.findtruck.fragment.KirimFragment;
import sragen.findtruck.fragment.TugasFragment;
import sragen.findtruck.model.Trip;
import sragen.findtruck.model.User;
import sragen.findtruck.services.AndroidLocationServices;
import sragen.findtruck.services.LocationUpdatesBroadcastReceiver;
import sragen.findtruck.services.MyAlarmService;
import sragen.findtruck.utilities.BaseActivity;
import sragen.findtruck.utilities.SharedPreference;
import sragen.findtruck.utilities.Utils;

/**
 * Created by adikurniawan on 03/05/18.
 */

public class HomeActivity extends BaseActivity {

    private TugasFragment tugasFragment;
    private AngkutFragment angkutFragment;
    private KirimFragment kirimFragment;
    private Adapter adapterFragment;
    private Bundle args;

    public static String TUGAS="DITUGASKAN";
    public static String ANGKUT="DIANGKUT";
    public static String KIRIM="TERKIRIM";


    private ProgressDialog progressDialog;
    private User user=null;

    private ViewPager viewPager;
    private Integer goToPage = null;

    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        setFCM();
        getBundle();

        cekLogin();
        setPager();
        setContent();
        setNavigation();


        //startTrackingService();
    }


    private void getBundle(){
        Bundle b= getIntent().getExtras();
        try{
            goToPage = b.getInt("page");
        }catch (Throwable throwable){}
    }

    private void cekLogin(){
        try{
            user=(User) SharedPreference.Get(getApplicationContext(),getString(R.string.data_user),User.class);
        }catch (Throwable throwable){}
        if(user==null){

            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

        }
    }

    private void setContent(){
        TextView tv_driver_name=(TextView)findViewById(R.id.tv_driver_name);
        if(user!=null) {
            tv_driver_name.setText(user.name);
        }
    }

    private void setNavigation(){
        findViewById(R.id.btn_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogout();
            }
        });

        if(goToPage!=null){
            viewPager.setCurrentItem(goToPage,true);
        }
    }



    private void setPager(){

        adapterFragment = new Adapter(getSupportFragmentManager());

        tugasFragment = new TugasFragment();
        args = new Bundle();
        args.putString("page", TUGAS);
        tugasFragment.setArguments(args);
        adapterFragment.addFragment(tugasFragment, TUGAS);

        angkutFragment = new AngkutFragment();
        args = new Bundle();
        args.putString("page", ANGKUT);
        angkutFragment.setArguments(args);
        adapterFragment.addFragment(angkutFragment, ANGKUT);

        kirimFragment = new KirimFragment();
        args = new Bundle();
        args.putString("page", KIRIM);
        kirimFragment.setArguments(args);
        adapterFragment.addFragment(kirimFragment, KIRIM);



        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(adapterFragment);

        TabLayout viewPagerTab = (TabLayout) findViewById(R.id.tab_layout);
        viewPagerTab.setupWithViewPager(viewPager);
    }


    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    /**
     * menampilkan progress dialog
     */
    protected void showProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(this);
            }
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }catch (Throwable th){}
    }

    /**
     * sembunyikan progress dialog
     */
    protected void dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }catch (Throwable te){}
    }

    private void setFCM(){
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(ConfigNotif.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(ConfigNotif.TOPIC_GLOBAL);

                    SharedPreferences pref = getApplicationContext().getSharedPreferences(ConfigNotif.SHARED_PREF, 0);
                    String regId = pref.getString("regId", null);

                    Log.e("Home", "Firebase reg id: " + regId);


                } else if (intent.getAction().equals(ConfigNotif.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };

    }




}
