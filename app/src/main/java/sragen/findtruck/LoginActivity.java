package sragen.findtruck;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import sragen.findtruck.app.API;
import sragen.findtruck.app.ConfigNotif;
import sragen.findtruck.app.ErrorCallback;
import sragen.findtruck.app.ResponseCallback;
import sragen.findtruck.model.User;
import sragen.findtruck.utilities.SharedPreference;

/**
 * Created by adikurniawan on 03/05/18.
 */

public class LoginActivity extends AppCompatActivity {
    EditText et_phone, et_pass;
    ProgressDialog progressDialog;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        setFCM();
        setContent();
        setNavigation();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void setFCM(){
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(ConfigNotif.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(ConfigNotif.TOPIC_GLOBAL);

                    SharedPreferences pref = getApplicationContext().getSharedPreferences(ConfigNotif.SHARED_PREF, 0);
                    String regId = pref.getString("regId", null);

                    Log.e("Home", "Firebase reg id: " + regId);


                } else if (intent.getAction().equals(ConfigNotif.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };

    }

    private void setContent(){
        et_phone=(EditText)findViewById(R.id.et_phone);
        et_pass=(EditText)findViewById(R.id.et_password);

//        et_phone.setText("089899911100");
//        et_pass.setText("12345678");
    }

    private void setNavigation(){
        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin();
            }
        });
    }

    private void doLogin(){
        showProgressDialog();
        API.login(this, et_phone.getText().toString(),
                et_pass.getText().toString(), new ResponseCallback() {
                    @Override
                    public int doAction(String response, int httpCode) {
                        dismissProgressDialog();

                        try {
                            JSONObject responseObj=new JSONObject(response);

                            if(httpCode==200) {
                                String token = responseObj.getString("token");

                                User user=new User(responseObj.getJSONObject("user"),token);

                                SharedPreference.Save(LoginActivity.this, getString(R.string.data_user), user);


                                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                                finish();


                            }else{
                                String message=responseObj.getString("message");
                                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                            }



                        } catch (JSONException e) {
                            dismissProgressDialog();
                            e.printStackTrace();
                        }
                        return httpCode;
                    }
                }, new ErrorCallback() {
                    @Override
                    public void doAction() {
                        dismissProgressDialog();
                    }
                });
    }

    /**
     * menampilkan progress dialog
     */
    protected void showProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(this);
            }
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }catch (Throwable th){}
    }

    /**
     * sembunyikan progress dialog
     */
    protected void dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }catch (Throwable te){}
    }
}
