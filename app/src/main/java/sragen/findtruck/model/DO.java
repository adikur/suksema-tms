package sragen.findtruck.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class DO implements Serializable {
    public String spk_number;
    public String manifest;
    public String qty;
    public String do_number;
    public String req_pick_up_date;
    public String req_delivery_date;
    public String origin_area;
    public String origin_name;
    public String origin_address;
    public String destination_area;
    public String destination_name;
    public String destination_address;
    public String do_status;
    public String status;

    public Double origin_latitude=0.0;
    public Double origin_longitude=0.0;
    public Double destination_latitude=0.0;
    public Double destination_longitude=0.0;

    public DO (JSONObject data) throws JSONException{
        this.spk_number=data.getString("spk_number");
        this.manifest=data.getString("manifest");
        this.qty=data.getString("qty");
        this.do_number=data.getString("do_number");
        this.req_pick_up_date=data.getString("req_pick_up_date");
        this.req_delivery_date=data.getString("req_delivery_date");
        this.origin_area=data.getString("origin_area");
        this.origin_name=data.getString("origin_name");
        this.origin_address=data.getString("origin_address");
        this.destination_area=data.getString("destination_area");
        this.destination_name=data.getString("destination_name");
        this.destination_address=data.getString("destination_address");
        this.do_status=data.getString("do_status");
        this.status=data.getString("status");

        if(!data.isNull("origin_latitude"))this.origin_latitude=Double.parseDouble(data.getString("origin_latitude"));
        if(!data.isNull("origin_longitude"))this.origin_longitude=Double.parseDouble(data.getString("origin_longitude"));
        if(!data.isNull("destination_latitude"))this.destination_latitude=Double.parseDouble(data.getString("destination_latitude"));
        if(!data.isNull("destination_longitude"))this.destination_longitude=Double.parseDouble(data.getString("destination_longitude"));
    }

}
