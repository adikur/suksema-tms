package sragen.findtruck.model;

/**
 * Created by adikurniawan on 23/04/18.
 */

public class Location {
    public double latitude;
    public double longitude;
    public String current_time;


    public Location(){


    }

    public Location (double latitude,double longitude){
        this.latitude=latitude;
        this.longitude=longitude;
    }

    public Location (double latitude,double longitude,String current_time){
        this.latitude=latitude;
        this.longitude=longitude;
        this.current_time=current_time;
    }
}
