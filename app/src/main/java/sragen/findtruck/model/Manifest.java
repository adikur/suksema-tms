package sragen.findtruck.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Manifest implements Serializable {
    public String id_manifest;
    public String origin_area;
    public String client_name;
    public String destination_area;
    public String delivery_date;
    public String vehicle_id;
    public String status_manifest;
    public String vehicle_type;
    public ArrayList<Order> dispatch_order =new ArrayList<Order>();

    public Manifest(JSONObject data) throws JSONException{
        this.id_manifest=data.getString("id_manifest");
        this.origin_area=data.getString("origin_area");
        this.client_name=data.getString("client_name");
        this.destination_area=data.getString("destination_area");
        this.delivery_date=data.getString("delivery_date");
        this.vehicle_id=data.getString("vehicle_id");
        this.vehicle_type=data.getString("vehicle_type");
        this.status_manifest=data.getString("status_manifest");

        JSONArray dis_arr=data.getJSONArray("dispatch_order");
        for(int i=0;i<dis_arr.length();i++){
            JSONObject order=dis_arr.getJSONObject(i);
            dispatch_order.add(new Order(order));
        }

    }

    public class Order implements Serializable{
        public String spk_number;
        public String qty;
        public String do_number;
        public String destination_area;
        public String do_status;
        public String status;

        public Order(JSONObject data) throws JSONException{
            this.spk_number=data.getString("spk_number");
            this.qty=data.getString("qty");
            this.do_number=data.getString("do_number");
            this.destination_area=data.getString("destination_area");
            this.do_status=data.getString("do_status");
            if(!data.isNull("status")) this.status=data.getString("status");
        }

    }
}
