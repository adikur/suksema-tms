package sragen.findtruck.model;

import java.io.Serializable;

public class Trip implements Serializable {
    public String spk_number;
    public String username;
    public String phone;

    public Trip(String spk_number, String username, String phone){
        this.spk_number=spk_number;
        this.username=username;
        this.phone=phone;
    }
}
