package sragen.findtruck.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by adikurniawan on 06/05/18.
 */

public class User {
    public String id;
    public String name;
    public String username;
    public String phone;
    public String photo;
    public String token;


    public User(JSONObject data,String token)throws JSONException{
        this.id=data.getString("id_driver");
        this.name=data.getString("driver_name");
        this.username=data.getString("driver_code");
        this.phone=data.getString("driver_code");
        this.token=token;
    }
}
