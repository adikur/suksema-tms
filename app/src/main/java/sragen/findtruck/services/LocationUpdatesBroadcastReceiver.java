package sragen.findtruck.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.LocationResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import sragen.findtruck.app.API;
import sragen.findtruck.app.ErrorCallback;
import sragen.findtruck.app.ResponseCallback;
import sragen.findtruck.app.TruckApp;
import sragen.findtruck.model.Trip;
import sragen.findtruck.utilities.Utils;

public class LocationUpdatesBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "LUBroadcastReceiver";

    public static final String ACTION_PROCESS_UPDATES =
            "com.suksema.tms.action" +
                    ".PROCESS_UPDATES";


    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PROCESS_UPDATES.equals(action)) {
                LocationResult result = LocationResult.extractResult(intent);
                if (result != null) {
                    List<Location> locations = result.getLocations();
                    Utils.setLocationUpdatesResult(context, locations);
                    Utils.sendNotification(context, Utils.getLocationResultTitle(context, locations));
                    Log.i(TAG, Utils.getLocationUpdatesResult(context));


                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd+HH:mm");
                    Date date = new Date();
                    setUp();

                    for (Location location : locations) {
                        sendLocationAPI(location.getLatitude(),location.getLongitude(),dateFormat.format(date).toString());
                        sendLocation(location.getLatitude(),location.getLongitude(),dateFormat.format(date).toString());
                    }

                }
            }
        }
    }




    private void sendLocationAPI(Double lat, Double lng,  String current_time){
        API.sendLocation(TruckApp.getAppContext(),lat, lng,current_time,  new ResponseCallback() {
            @Override
            public int doAction(String response, int httpCode) {
                try {

                    if(httpCode==200) {
                        Log.d(TAG, "has been send");
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return httpCode;
            }
        }, new ErrorCallback() {
            @Override
            public void doAction() {
            }
        });
    }


    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private String userId;


    public void setUp(){
        mFirebaseInstance = FirebaseDatabase.getInstance();

        // get reference to 'location' node
        mFirebaseDatabase = mFirebaseInstance.getReference("locations");

        // store app title to 'app_title' node
        mFirebaseInstance.getReference("app_title").setValue("Tracking Location");

        // app_title change listener
        mFirebaseInstance.getReference("app_title").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG, "App title updated");

                String appTitle = dataSnapshot.getValue(String.class);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read app title value.", error.toException());
            }
        });
    }

    private void sendLocation(double latitude, double longitude, String time) {
        // TODO
        // In real apps this userId should be fetched
        // by implementing firebase auth
        userId = mFirebaseDatabase.push().getKey();

        Date currentTime = Calendar.getInstance().getTime();
        Log.d("time", currentTime.toString());
        sragen.findtruck.model.Location user = new sragen.findtruck.model.Location(latitude, longitude,time);

        mFirebaseDatabase.child(userId).setValue(user);

        sendLocationListener();
    }



    private void sendLocationListener() {
        // User data change listener
        mFirebaseDatabase.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                sragen.findtruck.model.Location user = dataSnapshot.getValue(sragen.findtruck.model.Location.class);

                // Check for null
                if (user == null) {
                    Log.e(TAG, "User data is null!");
                    return;
                }

                Log.e(TAG, "User data is changed!" + user.latitude + ", " + user.longitude);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

}
