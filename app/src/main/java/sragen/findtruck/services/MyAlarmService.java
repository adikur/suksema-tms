package sragen.findtruck.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import sragen.findtruck.R;
import sragen.findtruck.app.API;
import sragen.findtruck.app.ErrorCallback;
import sragen.findtruck.app.ResponseCallback;
import sragen.findtruck.model.DO;
import sragen.findtruck.model.Location;
import sragen.findtruck.model.Trip;
import sragen.findtruck.utilities.SharedPreference;
import sragen.findtruck.utilities.Utilities;

/**
 * Created by adikurniawan on 22/04/18.
 */

public class MyAlarmService extends Service {
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;


    private String TAG="alarm service";
    private String userId;


    @Override
    public void onCreate() {
    // TODO Auto-generated method stub

        //Toast.makeText(this, "MyAlarmService.onCreate()", Toast.LENGTH_LONG).show();

        setUp();
    }



    @Override
    public IBinder onBind(Intent intent) {
    // TODO Auto-generated method stub

        //Toast.makeText(this, "MyAlarmService.onBind()", Toast.LENGTH_LONG).show();

        return null;

    }



    @Override
    public void onDestroy() {
    // TODO Auto-generated method stub

        super.onDestroy();

        //Toast.makeText(this, "MyAlarmService.onDestroy()", Toast.LENGTH_LONG).show();

    }



    @Override
    public void onStart(Intent intent, int startId) {
    // TODO Auto-generated method stub

        super.onStart(intent, startId);

        Toast.makeText(this, "Send location ...", Toast.LENGTH_LONG).show();


        Location location= (Location) SharedPreference.Get(getApplicationContext(), getString(R.string.my_coordinate), Location.class);
        Trip trip= (Trip) SharedPreference.Get(getApplicationContext(), getString(R.string.my_trip), Trip.class);

        if(location!=null && trip!=null) {
            if(Utilities.isOnline()){
                Log.d("send", location.latitude + ", " + location.longitude);

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date = new Date();
                String current_time=dateFormat.format(date).toString();

                //sendLocationAPI(location, trip, current_time);
                //sendLocation(location.latitude,location.longitude, current_time);
            }else{
                Toast.makeText(this, "connection failed", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(this, "location null", Toast.LENGTH_LONG).show();
        }
    }



    @Override
    public boolean onUnbind(Intent intent) {
    // TODO Auto-generated method stub

        //Toast.makeText(this, "MyAlarmService.onUnbind()", Toast.LENGTH_LONG).show();

        return super.onUnbind(intent);

    }


    public void setUp(){
        mFirebaseInstance = FirebaseDatabase.getInstance();

        // get reference to 'location' node
        mFirebaseDatabase = mFirebaseInstance.getReference("locations");

        // store app title to 'app_title' node
        mFirebaseInstance.getReference("app_title").setValue("Tracking Location");

        // app_title change listener
        mFirebaseInstance.getReference("app_title").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG, "App title updated");

                String appTitle = dataSnapshot.getValue(String.class);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read app title value.", error.toException());
            }
        });
    }

//    private void sendLocationAPI(Location location, Trip trip,String current_time){
//        API.sendLocation(this,location,current_time,  new ResponseCallback() {
//            @Override
//            public int doAction(String response, int httpCode) {
//                try {
//
//                    if(httpCode==200) {
//                        Log.d(TAG, "has been send");
//                    }
//                } catch (Throwable e) {
//                    e.printStackTrace();
//                }
//                return httpCode;
//            }
//        }, new ErrorCallback() {
//            @Override
//            public void doAction() {
//            }
//        });
//    }


    private void sendLocation(double latitude, double longitude, String time) {
        // TODO
        // In real apps this userId should be fetched
        // by implementing firebase auth
        userId = mFirebaseDatabase.push().getKey();

        Date currentTime = Calendar.getInstance().getTime();
        Log.d("time", currentTime.toString());
        sragen.findtruck.model.Location user = new sragen.findtruck.model.Location(latitude, longitude,time);

        mFirebaseDatabase.child(userId).setValue(user);

        sendLocationListener();
    }

    private void sendLocationListener() {
        // User data change listener
        mFirebaseDatabase.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                sragen.findtruck.model.Location user = dataSnapshot.getValue(sragen.findtruck.model.Location.class);

                // Check for null
                if (user == null) {
                    Log.e(TAG, "User data is null!");
                    return;
                }

                Log.e(TAG, "User data is changed!" + user.latitude + ", " + user.longitude);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }




}